package gserver;

import java.io.*;
import java.util.*;
import java.text.*;
import java.net.*;

import com.sun.net.httpserver.*;

import gasm.*;
import mpgenerator.*;

/* todo
    * read requests: allocate less / dynamic buffer
    * windows: java console ?
    * save gjs file
    * mps sources: replace=1 : replace audio script, without restarting sink or phase data source
*/

// *****************************************************************************
// ******* GServerHttpHandler **************************************************
// *****************************************************************************
class GServerHttpHandler implements HttpHandler {
// Stores gasm results for logging
    private static String gasmLogPath = null;
    private static Integer gasmLogErrors = 0;
    private static StringBuilder gasmLogMsgs = null;

// ******* htmlHeader **********************************************************
    private StringBuilder htmlHeader ( String title )  {
	StringBuilder sb = new StringBuilder();
	sb.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n");
	sb.append("<html>\n");
	sb.append("<head>\n");
	sb.append("  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf8\">\n");
	sb.append("<title>" + title + "</title>\n");
	sb.append("<style type=\"text/css\">\n");
	sb.append("body { background-color:#202020; color:#f0e0d0; font-family:Helvetica,Helv,sans-serif; font-size:2vh}\n");
	sb.append("a:link { color:#8080ff; }\n");
	sb.append("a:visited { color:#ff8080; }\n");
	sb.append("a:active { color:#80ff80; }\n");
	sb.append("button { background-color:#606060; color:#f8f0e0; font-size:110%; padding:7px 14px; border:none; border-radius:8px; cursor: pointer; transition: 0.5s;}\n");
	sb.append("button:hover { background-color:#707070 }\n");
	sb.append("div.log { font-size:1.5vh; background-color:#000000; text-align:left; padding:2vh }\n");
	sb.append("</style>\n");
	if ( title!=null ) sb.append("<title>"+title+"</title>\n");
	sb.append("</head>\n");
	sb.append("<body><center>\n");
	if ( title!=null ) sb.append("<h1>"+title+"</h1>\n");
        return sb;
    }

// ******* htmlConvert *********************************************************
    private byte[] htmlConvert ( StringBuilder sb )  { 
	sb.append ( "</center></body></html>" );
	return sb.toString().getBytes();
    }

// ******* findFile ************************************************************
    // finds case insensitve name of file or directory within directory. Directory is relative to GServer.dataDir.
    // dir must start with a separator if not empty
    // return the filename or null id no file has been found
    private String findFile ( String dir, String name )  { 
	String fd = ( (dir!=null) && (dir.length()>0) ) ? GServer.dataDir + dir : GServer.dataDir;
	if ( new File(fd,name).exists() ) return name;
	String[] list = new File(fd).list();
	if ( list == null ) return null;
	for ( int i=0; i<list.length; i++ )
	    if ( list[i].equalsIgnoreCase(name) ) return list[i];
	return null;
    }
    
// ******* accessDenied ********************************************************
    private void accessDenied(HttpExchange t, String msg ) throws IOException {
	StringBuilder sb = htmlHeader ("400 Bad Request");
	sb.append("Access denied" );
	if ( msg != null ) sb.append(": "+msg);
	byte[] buf = htmlConvert(sb);
	t.sendResponseHeaders(400, buf.length);
	t.getResponseHeaders().add("Content-Type", "text/html;Charset=utf8");
        OutputStream os = t.getResponseBody();
        try {
    	    os.write(buf);
        } catch ( IOException e ) {
	    GLogger.warning("Connection to client lost: "+ e.getLocalizedMessage());
    	    return;
    	}
        os.close();
        GLogger.info("Rejecting connection from " + IPPermission.toString( t.getRemoteAddress().getAddress() ) );
    }

// ******* notFound ************************************************************
    private void notFound(HttpExchange t, String path ) throws IOException {
	StringBuilder sb = htmlHeader ("404 Not Found");
	if ( path!=null ) sb.append("Invalid path: " + path );
	byte[] buf = htmlConvert(sb);
	t.sendResponseHeaders(404, buf.length);
	t.getResponseHeaders().add("Content-Type", "text/html;Charset=utf8");
        OutputStream os = t.getResponseBody();
        try {
    	    os.write(buf);
        } catch ( IOException e ) {
	    GLogger.warning("Connection to client lost: "+ e.getLocalizedMessage());
    	    return;
    	}
        os.close();
        if ( path != null ) GLogger.info("Not found (from " + IPPermission.toString( t.getRemoteAddress().getAddress() ) +"): '" + path + "'" );
    }

    
// ******* decodeURLParams *****************************************************
    private static void decodeURLParams(AbstractList<String[]> params, String encParams) throws UnsupportedEncodingException {
	if ( (encParams==null) || (encParams.length()<1) ) return;
	String[] sa = encParams.split("&");
	for ( int i=0; i<sa.length; i++ ) {
	    String[] pair = sa[i].split("=",2);
	    for ( int j=0; j<pair.length; j++ ) 
		pair[j] = URLDecoder.decode(pair[j],"UTF-8");
	    params.add(pair);
//	    System.out.println("decodeURLParams: " + pair[0] + ( (pair.length>1) ? " = " + pair[1] : "" ) );
	}
    }

// ******* legalFile ***********************************************************
    // replace illegal character '/', '\' and ':' by '_'
    private String legalFile ( String fn )  { 
	return fn.replace('\\','_').replace('/','_').replace(':','_');
    }

// ******* sendFile ************************************************************
    // return file contents as response
    private void sendFile ( File file, HttpExchange t) throws IOException {
	int remaining = (int) file.length();
	byte[] b = remaining > 0 ? new byte[Math.min(1024*1024,remaining)] : null;	// 1 M file buffer
    	t.sendResponseHeaders(200, file.length());
    	OutputStream os = t.getResponseBody();
    	InputStream is = new FileInputStream(file);
    	while ( remaining > 0) {
    	    int l = Math.min(b.length,remaining);
    	    is.read(b,0,l);
    	    try {
    		os.write(b,0,l);
    	    } catch ( IOException e ) {
    		GLogger.warning("Connection to client lost: "+ e.getLocalizedMessage());
    		return;
    	    }
    	    remaining -= l;
    	}
    	os.close();
    }

    
// ******* handle **************************************************************
    public void handle(HttpExchange t) throws IOException {
	final int maxInSize = 256*1014;		// maximum size for POST data
    
	if ( ! GServer.permissions().checkAddress( t.getRemoteAddress().getAddress() ) ) {
	    accessDenied(t,null);
    	    return;
	}

	int responseCode = 200;
	byte buf[] = {};
	LinkedList<String[]> params = new LinkedList<String[]>();	// list of key/value pairs. Length of the array is 1 or 2
	String path = t.getRequestURI().getPath();
	InputStream in = t.getRequestBody();
	int rcvd = 0;
	MPContext context = GServer.audio.context();
	if ( in.available()>0 ) {
	    try {
		int i;
	        byte[] inbuf = new byte[maxInSize]; 
	        do {
		    i = in.read(inbuf, rcvd, maxInSize-rcvd);
		    if ( i>0 ) rcvd += i;
		} while ( (rcvd<maxInSize) && (i>0) && (in.available()<1) );
		if ( rcvd<1 ) GLogger.warning("Unable to read request data");
		else decodeURLParams( params, new String(inbuf,0,rcvd) );
		in.close();
	    } catch ( Exception e ) {
	        GLogger.error("Error reading / encoding request data:" + e.getLocalizedMessage() );
	    }
	}
	try {
	    // decode params
	    decodeURLParams( params, t.getRequestURI().getRawQuery());
	    // normalize path
	    if ( (path.length()>1) && path.charAt(0)=='/'  && path.charAt(1)==':' ) path = path.substring(1);
	    else if (path.charAt(0)!='/') path = '/' + path;
	    if ( path.charAt(path.length()-1) == '/' ) path += "index.html";
	    String dir = "";		// normalized directory of the path relative to GServer.dataDir and starting with separator. Must not contain ':' because colon is the directory separator under windows. '/' and '\' are interpreted as directory seperators
	    String name = null;		// file name of the path or null if non-existent
	    String gName = null;	// gasm script file (suffix ".gsc") if suffix is ".gjs"
	    String suffix = ""; 	// suffix of the path, lowercase
	    boolean isIndex = false;	// index file, may be virtual
	    File file = null;		// file object or null if non-existent
	    if ( path.charAt(0) != ':' ) {
		if (path.indexOf(':') > 0 ) {
		    accessDenied(t,"Malformed path. Colon (`:') is not allowed.");
    		    return;
		}
		String[] sa = path.replace('\\','/').split("/");
		int i=0;
		for ( int j=0; j<sa.length; j++ ) {
		    if ( sa[j].equals("..") ) {
		        if ( i>0 ) i--;
		    } else if ( (sa[j].length()>0) && !sa[j].equals(".") ) {
			if ( j!=i ) sa[i] = sa[j];
		        i++; 
		    }
		}
		if ( i < 1 ) {
		    accessDenied(t,"Malformed path: `" + path + "'");
    		    return;
		}
		for ( int j=0; j<i-1; j++ ) {
		    String n = findFile(dir,sa[j]);
		    if ( n == null ) {
		        notFound(t,path);
		        return;
		    }
		    dir += File.separator + n;
		}
		String n = sa[i-1];
		i = n.lastIndexOf('.');
		if ( i > 0 ) suffix = n.substring(i+1).toLowerCase();
		name = findFile(dir,n);
		if ( name != null ) {
		    file = new File(GServer.dataDir + dir, name);
		    if ( ! file.isFile() ) file = null;
		}
	    	if ( suffix.equals("gjs") && ( (name==null) || !GServer.disableJITAssembler ) ) gName = findFile( dir, n.substring(0,i)+".gsc" );
		else if ( suffix.equals("gsc") ) gName = name;
		isIndex = n.equalsIgnoreCase("index.html") || n.equalsIgnoreCase("index.htm");
	    }
	    // process pages
	    if ( path.equalsIgnoreCase(":stopAudio") ) {				// abort audio playback
		GServer.audio.setAudio(null,null);	
		t.getResponseHeaders().add("Content-Type", "text/plain; Charset=utf8");
		buf = "OK".getBytes();
	    } else if ( path.equalsIgnoreCase(":savejson") ) {				// save json data to file
		t.getResponseHeaders().add("Content-Type", "text/plain; Charset=utf8");
		for ( String[] p : params)
		    if ( (p.length>1) && (p[0].length()>0) && (p[1].length()>0) ) {
			String jsonFile = context.configDir() + File.separatorChar + legalFile(p[0]) + ".json";
			try {
    			    PrintStream ps = new PrintStream( jsonFile );
    			    ps.println(p[1]);
    			    ps.close();
		    	    GLogger.info("Wrote " + p[1].length() + " bytes to '" + jsonFile + "'");
			} catch ( Exception e ) {
		    	    GLogger.error("Error writing to '" + jsonFile + "': " + e.getLocalizedMessage());
			}
		}
		buf = "OK".getBytes();
	    } else if ( path.equalsIgnoreCase(":loadjson") ) {				// only one mandatory parameter is evaluated: name. If file does not exist an empty string is returned.
	        t.getResponseHeaders().add("Content-Type", "application/json;Charset=utf8");
		String n = null;
		for ( String[] p : params)
		    if ( (p.length>1) && p[0].equalsIgnoreCase("name") && (p[1].length()>0) ) 
			n = p[1];
		if ( n == null ) GLogger.warning("Warning: ':loadjson' used without parameter 'name'");
		else {
		    String jsonFile = context.configDir() + File.separatorChar + legalFile(n) + ".json";
		    File f = new File( jsonFile );
		    if ( f.isFile() ) {
			try {
			    sendFile(f,t);
			    return;
			} catch ( Exception e ) {
		    	    GLogger.error("Error reading file '" + jsonFile + "': " + e.getLocalizedMessage());
			}
		    }
		}
	    } else if ( path.equalsIgnoreCase(":gasmlog") ) {				// gasm message page
		t.getResponseHeaders().add("Content-Type", "text/html;Charset=utf8");
		StringBuilder sb ;
		if ( (gasmLogPath!=null) && (gasmLogMsgs!=null) ) {
		    sb = htmlHeader (gasmLogPath+": "+gasmLogErrors+" errors");
		    sb.append("<div class=\"log\"><pre>" + gasmLogMsgs + "</pre></div>" );
		} else {
		    sb = htmlHeader( "No gasm messages" );
		    sb.append("No errors or warnings occured");
		}
	        sb.append("<p><button onclick=\"window.history.back()\">Go Back</button>");
		buf = htmlConvert(sb);
	    } else if ( path.equalsIgnoreCase(":logtext") ) {				// log as plain text
		t.getResponseHeaders().add("Content-Type", "text/plain;Charset=utf8");
		buf = GLogger.logbuf.toString().getBytes();
	    } else if ( path.equalsIgnoreCase(":print") ) {				// prints a message to log, Supported message types (=key) are msg,error,warning,info. All other keys are ignored
		for ( String[] p : params)
		    if ( p.length > 1 ) {
			if ( p[0].equalsIgnoreCase("msg") ) GLogger.msg(p[1]);
			else if ( p[0].equalsIgnoreCase("error") ) GLogger.error(p[1]);
			else if ( p[0].equalsIgnoreCase("warning") ) GLogger.warning(p[1]);
			else if ( p[0].equalsIgnoreCase("info") ) GLogger.info(p[1]);
		    }
		t.getResponseHeaders().add("Content-Type", "text/plain; Charset=utf8");
		buf = "OK".getBytes();
	    } else if ( path.equalsIgnoreCase(":log") ) {				// print log and stop server if key parameter "stop" exist not equal "0"
		boolean stop = false;
		for ( String[] p : params)
		    stop = stop || ( (p.length>1) && p[0].equalsIgnoreCase("stop") && p[1].equals(GServer.querySecret) );
		if ( stop ) GLogger.msg("Stopping server ...");
		t.getResponseHeaders().add("Content-Type", "text/html;Charset=utf8");
		StringBuilder sb;
		if ( stop ) sb = htmlHeader ( "Stopping GServer" );
		else  sb = htmlHeader ( "GServer log" );
	        sb.append("<div class=\"log\"><pre>" + GLogger.logbuf + "</pre></div>" );
	        if ( !stop ) sb.append("<p><div id=\"bottom\"><a href=\"javascript:history.go(0)\"><button>Reload</button></a> &nbsp;&nbsp;&nbsp; <a href=\"/index.html\"><button>Go Back</button></a> &nbsp;&nbsp;&nbsp; <a href=\"%3Alogtext\" download=\"GServer.log\"><button>Download Log</button></a> &nbsp;&nbsp;&nbsp; <a href=\"/index.html?clearlog="+GServer.querySecret+"\"><button>Clear Log</button></a><div>");
	        else sb.append("<p><div id=\"bottom\"></div>");
		buf = sb.toString().getBytes();
		if ( stop ) GServer.run=false;
	    } else if ( path.equalsIgnoreCase(":setConfig") ) {				// sets configuration variables and returns errors
		boolean save = false;
		StringBuilder sb = new StringBuilder();
		int errors = 0;
		for ( String[] p : params) {
		    if ( (p.length<2) || (p[0].length()<1) || (p[1].length()<1) ) {
			errors++;
			sb.append("Invalid name - value pair for key '"+p[0] + "'\n");
			continue;
		    }
		    GLogger.info("Set configuration: " + p[0] + "=" + p[1]);
		    String err = context.setConfig(p[0],p[1]);
		    if ( err != null ) {
			errors++;
			sb.append("Error setting configuration '" + p[0] + "' to '" + p[1] + "': " + err + "\n");
		    }
		}
		String err = context.testConfig();
		if ( err != null ) {
		    errors++;
		    sb.append("Configuration error: " + err + "\n");
		}
		if ( errors == 0 ) {
		    sb.append("Saving configuration to file '"+GServer.audio.context().configFile() + "'\n");
		    try {
			GServer.audio.context().writeConfigFile();
		    } catch ( Exception e ) {
			errors++;
			sb.append("Error saving configuration file '"+GServer.audio.context().configFile() + "':" + e.getLocalizedMessage() + "\n");
		    }
		}
		if ( errors > 0 ) {
	    	    GLogger.warning("" + errors + " errors occurred while reading configuration");
	    	    if ( !GLogger.quiet ) GLogger.print(sb.toString());
		}
		else {
		    if ( GLogger.verbose ) GLogger.print(sb.toString());
		}
		t.getResponseHeaders().add("Content-Type", "text/plain; Charset=utf8");
		buf = sb.toString().getBytes();
	    } else if ( path.equalsIgnoreCase(":getconfig") ) {				// returns configuration variables as json object. One parameter is supporterd: configName, the configuration name
		for ( String[] p : params) {
		    if ( (p.length>1) && p[0].equalsIgnoreCase("configName") ) {
			 context.setConfigName( p[1] );
			 GLogger.info("Loaded new configuration file '"+p[1] + "'" );
		    };
		}
		t.getResponseHeaders().add("Content-Type", "application/json;Charset=utf8");
		StringBuilder sb = new StringBuilder();
		sb.append("{ \"configName\":\"").append(context.configName()).append("\"");
		sb.append(" , \"configDir\":\"").append(context.configDir()).append("\"");
		Map<String,String> map = context.getConfigs();
		for ( String k : map.keySet() ) {
		    sb.append(" , ").append("\"").append(k).append("\":\"").append(map.get(k)).append("\"");
		}
		sb.append(" }");
		buf = sb.toString().getBytes();
	    } else if ( gName!=null ) {							// compile gasm script
		String fn = GServer.dataDir + dir + File.separator + gName;
		GLogger.info("Compiling gasm script `" + fn + "'" );
		LinkedList<GASnippet> script = new LinkedList<GASnippet>();
		StringBuilder sb = new StringBuilder();
		GAsm gasm = new GAsm(GServer.dataDir);
		int errors = gasm.asm(fn, script, sb);	// data dir is stripped automatically
		errors += gasm.asm2(script, sb);
	        if ( errors>0 ) GLogger.error("" + errors + " errors occurred while compiling `" + fn + "'");
		if ( sb.length() > 0 ) {
		    gasmLogPath = fn;
		    gasmLogErrors = errors;
		    gasmLogMsgs = sb;
		    if ( (errors>0) || !GLogger.quiet ) GLogger.print(sb.toString());
		     sb = new StringBuilder();
		}
		t.getResponseHeaders().add("Content-Type", "text/javascript;Charset=utf8");
		if ( errors>0 ) sb.append("window.location.href = '%3Agasmlog';\n");
		else gasm.write(script,sb);
		buf = sb.toString().getBytes();
	    } else if ( path.equalsIgnoreCase(":mps") || (suffix.equals("mps") && (file!=null)) ) {			// play sound; return error messages or "OK"
		int posMax = 0;
		// scan parameters (without parsing variables)
		String scr = null;
		if ( file == null ) {
		    for ( String[] p : params) 
			if ( (p.length>1) && p[0].equalsIgnoreCase("scr") && (p[1].length()>0) ) 
			    scr = p[1];
		}
		if ( file!=null || scr!=null ) context.userVarsUnInit();
		//parse variables
		HashMap<Integer,Double> ppm = new HashMap<Integer,Double>();	// positional parameters
		StringBuilder msgs = new StringBuilder();			// parser errors, will be returned
		for ( String[] p : params) {
		    if ( (p.length>1) && (p[0].length()>1) && (p[1].length()>0) ) {
			char c = p[0].charAt(0);
			double v = 0;
			String err = null;
			if ( c=='$' || c=='@' ) {
			    try {
				v = context.eval(p[1],null);
			    }
			    catch ( ParseException pe ) {
				err = context.getParseExceptionMessage("Error parsing value for variable/parameter '" + p[0] + "'",p[1],pe);
			    }
			    if ( err==null && c=='$' ) {		// positional parameter
				int n=0;
				try {
	    			n = Integer.parseInt( p[0].substring(1) );
	    			}
	    			catch ( Exception e ) {  }
	    			if ( n < 1 ) err = "Invalid name of positional parameter: '"+p[0]+"'";
	    			else {
	    			    posMax = Math.max(n,posMax);
	    			    ppm.put(n,v);
	    			}
	    		    } else if ( err==null ) {
	    		        try {
	    		    	    context.setUserVar( p[0].substring(1), v);
	    		    	    GLogger.info(p[0]+"="+v);
	    			}
				catch ( Exception e ) {
				    err = "Invalid user variable '" + p[0] + "': " + e.getLocalizedMessage();
				}
			    }
			}
			if ( err!=null ) {
			    GLogger.warning("Warning: "+err);
			    msgs.append(err).append("\n");
			}
		    } 
		}
		// compose array of positional parameters
		double[] pp = null;
		if (posMax>0) {
		    pp = new double[posMax];
		    for ( int i=1; i<=posMax; i++ ) {
			Double v = ppm.get(i);
			pp[i] = (v == null) ? 0 : v;
		    }
		}	
		// load audio script
		try {
	    	    if ( file!=null ) GServer.audio.setAudio(file.getPath(),pp);
	    	    else if (scr!=null ) GServer.audio.setAudio(scr,GServer.dataDir,pp);	
	    	}
	    	catch ( Exception e ) {
	    	    String err = "Error reading audio script: " + e.getLocalizedMessage();
	    	    GLogger.warning(err);
	    	    msgs.append(err).append("\n");
	    	}
		t.getResponseHeaders().add("Content-Type", "text/plain; Charset=utf8");
		buf = (msgs.length()>0) ? msgs.toString().getBytes() : "OK".getBytes();
	    } else if ( file != null ) {
	        if ( suffix.equals("js") ) t.getResponseHeaders().add("Content-Type", "text/javascript;Charset=utf8");
	        else if ( suffix.equals("htm") || suffix.equals("html") ) t.getResponseHeaders().add("Content-Type", "text/html");
	        else if ( suffix.equals("txt") ) t.getResponseHeaders().add("Content-Type", "text/plain");
	        else if ( suffix.equals("json") ) t.getResponseHeaders().add("Content-Type", "application/json;Charset=utf8");
	        else if ( suffix.equals("jpeg") || suffix.equals("jpg") ) t.getResponseHeaders().add("Content-Type", "image/jpeg");
	        else if ( suffix.equals("png") ) t.getResponseHeaders().add("Content-Type", "image/png");
	        else if ( suffix.equals("gif") ) t.getResponseHeaders().add("Content-Type", "image/gif");
	        else if ( suffix.equals("mp3") ) t.getResponseHeaders().add("Content-Type", "audio/mpeg");
	        else if ( suffix.equals("ogg") || suffix.equals("oga") ) t.getResponseHeaders().add("Content-Type", "audio/ogg");
	        else if ( suffix.equals("wav") ) t.getResponseHeaders().add("Content-Type", "audio/wav");
	        else if ( suffix.equals("mp4") ) t.getResponseHeaders().add("Content-Type", "video/mp4");
	        else if ( suffix.equals("ogv") ) t.getResponseHeaders().add("Content-Type", "video/ogv");
	        else if ( suffix.equals("webm") ) t.getResponseHeaders().add("Content-Type", "video/webm");
	        sendFile(file,t);
    		return;
    	    } else if ( isIndex ) {
		// actions
		boolean cl = false;
		boolean sa = false;
		for ( String[] p : params) {
		    cl = cl || ( (p.length>1) && p[0].equalsIgnoreCase("clearlog") && p[1].equals(GServer.querySecret) );
		    sa = sa || ( (p.length>1) && p[0].equalsIgnoreCase("stopAudio") && p[1].equals("1") );
		}
		if ( cl ) GLogger.logbuf.setLength(0);
		if ( sa ) GServer.audio.setAudio(null,null);	
		// response
	        t.getResponseHeaders().add("Content-Type", "text/html;Charset=utf8");
	        StringBuilder sb = htmlHeader ("Index");
	        String[] files = FindFiles.webFiles.list(new File(GServer.dataDir + dir));
	        if ( files != null ) {
	    	    for ( int i=0; i<files.length; i++ ) {
	    		String f = files[i];
	    		int j = f.lastIndexOf('.');
	    		sb.append("<b><a href=\"" + f + "\">" + ( j<1 ? f : f.substring(0,j) ) + "</a></b><p>");
	    	    }
	        } else {
	    	    sb.append("<b>No HTML files found. Is data directory correct?</b><p>");
	        }
	        if ( path.equalsIgnoreCase("/index.html") ) {	// additional control button on top level index.html
	    	    sb.append("<p><div><a href=\"%3Alog#bottom\"><button>View Log</button></a>");
	    	    sb.append("&nbsp;&nbsp;&nbsp; <a href=\"%3Alogtext\" download=\"GServer.log\"><button>Download Log</button></a>");
	    	    if ( GServer.audio.isPlaying() ) sb.append("&nbsp;&nbsp;&nbsp; <a href=\"/index.html?stopAudio=1\"><button>Stop Audio</button></a>");
	    	    sb.append("&nbsp;&nbsp;&nbsp; <a href=\"%3Alog?stop="+GServer.querySecret+"#bottom\"><button>Stop GServer</button></a><div>");
	        } else System.out.println(path);
	        buf = htmlConvert(sb);
	    } else {
		notFound(t, path);
		return;
	    }
	} catch ( Exception e ) {
	    responseCode = 500;
	    t.getResponseHeaders().add("Content-Type", "text/html;Charset=utf8");
	    StringBuilder sb = htmlHeader ("Internal server error");
	    sb.append( "Internal server error: " + e.getLocalizedMessage() +"<br>Also see <a href=\"%3Alog\">GServer log</a>.");
	    buf = htmlConvert(sb);
	    MPLogger.stackTrace("Internal server errors:",e);
	    e.printStackTrace();
	}
        GLogger.info( "Connection from " + IPPermission.toString( t.getRemoteAddress().getAddress() ) + ": " + path + ": " + responseCode + ": received " + rcvd + " bytes,  sent " + buf.length + " bytes" );

        t.sendResponseHeaders(responseCode, buf.length);
        OutputStream os = t.getResponseBody();
	try {
    	    os.write(buf);
        } catch ( IOException e ) {
	    GLogger.warning("Connection to client lost: "+ e.getLocalizedMessage());
    	    return;
    	}
        os.close();
    }
}


// *****************************************************************************
// ******* IPPermission ********************************************************
// *****************************************************************************
class IPPermission {
    private byte ip[][] = { { 127, 0, 0, 1 } };
    private int mask[] = { 32 };
    
    public IPPermission(String adrs) throws UnknownHostException,IllegalArgumentException {
	String strs[] = adrs.split(",");
	ip = new byte[strs.length][];
	mask = new int[strs.length];
	for (int i=0; i<strs.length; i++ ) {
	    if ( strs[i].length()==0 ) throw new IllegalArgumentException( "Invalid address format at position " + (i+1) + ": empty string");
	    int end = strs[i].lastIndexOf("/");
	    if ( end < 0 ) end = strs[i].length();
	    ip[i] = InetAddress.getByName(strs[i].substring(0,end)).getAddress();
	    try {
		mask[i] = ( end+1 < strs[i].length() ) ? Integer.parseInt(strs[i].substring(end+1)) : ip[i].length*8;
	    }
	    catch (Exception e) {
		throw new IllegalArgumentException("Invalid mask format at position " + (i+1) + ": `" + strs[i].substring(end+1) + "'" );
	    }
	}
    }

    public IPPermission() {
    }
    
    public boolean checkAddress ( byte rip[]) {
	
	for ( int i=0; i<ip.length; i++ ) {
	    boolean b = ip[i].length == rip.length ;
	    int j = 0;
	    while (b && j<rip.length ) {
		int k = Math.max( (j+1)*8-mask[i], 0);
	    	b = b && ( (ip[i][j] & 255)>>k == (rip[j] & 255)>>k );
	    	j++;
	    }
	    if ( b ) return true;
	}
	return false;
    }

    public boolean checkAddress ( InetAddress adr ) {
	return checkAddress( adr.getAddress() );
    }
    
    public static String toString(byte ip[]) {
	StringBuilder sb = new StringBuilder();
	if ( ip.length<6 || (ip.length & 1) != 0 ) {
	    for (int i=0; i<ip.length; i++ ) {
		if (i>0) sb.append('.');
		sb.append(ip[i] & 255);
	    }
	}
	else {
	    for (int i=0; i+1<ip.length; i+=2 ) {
		if (i>0) sb.append(':');
		sb.append(Integer.toString( ((ip[i] & 255)<<8) | (ip[i+1] & 255), 16 ) );
	    }
	}
	
	return sb.toString();
    }

    public static String toString(InetAddress adr) {
	return toString( adr.getAddress() );
    }

    public String toString() {
	StringBuilder sb = new StringBuilder();
	for (int i=0; i<ip.length; i++ ) {
	    if (i>0) sb.append(',');
	    sb.append(toString(ip[i])+"/"+mask[i]);
	}
	return sb.toString();
    }
}

// *****************************************************************************
// ******* GLogger *************************************************************
// *****************************************************************************
/** Logs to stdout and to a buffer */
class GLogger extends MPLogger {
/** Log buffer. */
    public static StringBuffer logbuf = new StringBuffer();

/** Print infos. */
    public static boolean verbose = false;

/** Do not prit warnings. */
    public static boolean quiet = false;

    
/** newline. */
    private String nl;

/** 
  * Constructor
  */
    public GLogger () {
	super(System.out);	// makes the compiler happy
	nl = System.getProperty("line.separator");
    }

/** 
  * Message that is always printed
  */
    public static void msg (String msg) {
	log.println( msg );
    }

/** 
  * Error: always printed
  */
    public static void error (String msg) {
	log.println(msg);
    }

/** 
  * Warning: printed if quiet is false
  */
    public synchronized static void warning (String msg) {
	if ( !quiet ) log.println(msg);
    }

/** 
  * Info: printed if verbose is true
  */
    public synchronized static void info (String msg) {
	if ( verbose ) log.println(msg);
    }
    
/** 
  * Prints the message to stderr.
  * @param s The message to print
  */
    public synchronized void println(String s) {
	System.out.println(s);
	logbuf.append(s);
	logbuf.append(nl);
    }

/** 
  * Prints the message to stderr that already contains trailing newline.
  * @param s The message to print
  */
    public static synchronized void print(String s) {
	System.out.print(s);
	logbuf.append(s);
    }
    
    static {
	log = new GLogger();
	err = log;
    }
}


// *****************************************************************************
// ******* GServer ********************************************************
// *****************************************************************************
class GServer {
    public static String dataDir = ".";

    public static boolean disableJITAssembler = false;

    public static int port = 7980;
    public static boolean run = true;
    
    private static int verbose = 0;

    private static Vector<Socket> socketVector = new Vector<Socket>();
    
    private static IPPermission permissions = new IPPermission();
    private static String bind = null;
    
    public static MPAudioServer audio = null;

    public static String querySecret = Long.toString(Math.round(Math.random()*1e6-0.499));

    private static String confName = "-";	// configuration name at start
    
// ******* ParameterException **************************************************
    static class ParameterException extends Exception {
	final static String helpMsg = new String (
			"Usage: java -jar GServer.jar [<options>]\n"+
			"Options:\n"+
			"    -d <path>        Document directory (default: current directory)\n"+
			"    -c <name>        Configuration name (default: 'default')\n"+
			"    -p <port>        Port number for the HTTP interface (default: 7980)\n"+
			"    -a <address>[/<mask>][,...]  Allow HTTP connection from this address(es),\n"+
			"                     <mask> 24 is equivalent to 255.255.255.0, default: 127.0.0.1\n"+
			"    -b <address>     Bind HTTP server to this address (default: listen on all interfaces)\n"+
			"    -j               Disable JIT assembler if compiled scripts exist\n"+
			"    -v               Be verbose (can be specified two times for additional sound info)\n"+
			"    -q               Be quiet (suppress warnings)\n"+
			"    -a               Scan all interfaces (default: interface 0 only)\n"+
			"    -h               Help" );

	public ParameterException (String msg) {
	    super( msg + "\n" + helpMsg );
	}

	public ParameterException () {
	    super( helpMsg );
	}
    }
    
// ******* httpPermissions *****************************************************
    public static IPPermission permissions() {
	return permissions;
    }
    
// ******* sleep ***************************************************************
    public static void sleep(int ms) {
	try {
	    Thread.sleep(ms);
        }
	catch ( InterruptedException e ) {
	}
    }


// ******* main ****************************************************************
    public static void main (String args[]) {
	try {
	    // scan cmd line arguments
	    for (int i=0; i<args.length; i++ ) {
		if ( args[i].equals("-d") ) {
		    i++;
		    if (i>=args.length) throw new ParameterException("Data directory expected after -d");
		    dataDir = args[i];
		}
		else if ( args[i].equals("-c") ) {
		    i++;
		    if (i>=args.length) throw new ParameterException("Configuration name expected after -c");
		    confName = args[i];
		}
		else if ( args[i].equals("-p") ) {
		    i++;
		    try {
			if (i>=args.length) throw new Exception();
    			port = Integer.parseInt( args[i] );
		    } 
		    catch (Exception e) {
			throw new ParameterException("Port number expected after -p");
		    }
		}
		else if ( args[i].equals("-a") ) {
		    i++;
		    try {
			if (i>=args.length) throw new Exception("Argument expected after -a");
    			permissions = new IPPermission( args[i] );
		    } 
		    catch (Exception e) {
			throw new ParameterException("Error parsing HTTP permissions:" + e);
		    }
		}
		else if ( args[i].equals("-b") ) {
		    i++;
		    if (i>=args.length) throw new ParameterException("Argument expected after -b");
    		    bind = args[i];
		}
		else if ( args[i].equals("-j") ) {
		    disableJITAssembler = true;
		}
		else if ( args[i].equals("-v") ) {
		    verbose++;
		}
		else if ( args[i].equals("-q") ) {
		    GLogger.quiet = true;
		}
		else if ( args[i].equals("-h") ) {
		    System.out.println(ParameterException.helpMsg);
	    	    System.exit(0);
		}
		else {
		    throw new ParameterException("Invalid argument: "+args[i]);
		}
	    }

	    GLogger.verbose = verbose>0;

	    // convert data dir to absolute path
	    dataDir = new File(dataDir).getAbsolutePath();

	    // create and start audio server
	    audio = new MPAudioServer(verbose);
	    try {
		audio.context().setConfigName(confName);
	    } catch (Exception e) {
		GLogger.warning("Warning: "+e.getLocalizedMessage() );
	    }  
	    
	    // handling of interrupt signal: stop HTTP server and audio handler
	    Runtime.getRuntime().addShutdownHook(new Thread() {
		public void run() { 
		    GServer.run = false;
		    GServer.audio.terminate();
		}
	    });	

	    // start http server
	    HttpServer httpServer = null;
	    GLogger.info ( "Listening for HTTP connections at port " + port + " from addresses " + permissions );
    	    httpServer = HttpServer.create( ( bind == null ) ? new InetSocketAddress(port) : new InetSocketAddress(InetAddress.getByName(bind),port), 0);
    	    httpServer.createContext("/", new GServerHttpHandler());
    	    httpServer.setExecutor(null);
    	    httpServer.start();

	    // run http server
	    while ( run ) {
	        sleep(250);
	    }
	    sleep(500);	// some time to sende response

	    // stop audio server
	    GServer.audio.terminate();

	    // stop http server
	    if ( httpServer!=null ) httpServer.stop(1);
	    GLogger.info("Stopped HTTP server");
	    GServer.audio.terminate();
	} 
	catch ( ParameterException e ) {
	    System.err.println("Error: " + e.getLocalizedMessage() );
	} 
	catch (Exception e) {
	    MPLogger.stackTrace("Fatal Error:",e);
	}  
   } 
   
}
