gserver
=======

The name stands for guide server. It is a HTTP server intended for running interactive web guides
and has two main extensions:

1. Generation and playback of EStim sound files
2. JIT [gasm](https://gitlab.com/mantrid0/mpgenerator/blob/master/gasm/Readme.md) -- 
   integrated guide assembler

Usage
-----

Java is required to run gserver.

### Quick usage

Copy `GServer.jar` to the directory with the start file(s) (HTML file(s)) of the web guide(s) and
double click on it in a file browser. The server runs in background. Open a web browser and go to 
`127.0.0.1:7980` or `localhost:7980` (the latter may not work on all OS)

### Not so quick usage

In order to see what is going on, start GServer in a console with the command 

```java -jar GServer.jar -v -d <document directory>```

where `<document directory>` is the directory with the start file(s) of the web guide(s). The option 
`-d ...` can be omitted if gserver is started from the document directory.

### Full usage

Command line syntax is 

```java -jar GServer.jar [<options>]```

#### Options

##### `-d <path>`
Document directory. Default: current directory.

##### `-c <name>`
Set the initial name of the audio configuration. Default: "default".

##### `-p <port>`
Port number for the HTTP interface. Default: 7980

##### `-a <address>[/<mask>][,...]`
Comma separated list of IP addresses from which connections are accepted. A `<mask>` value of
24 is equivalent to the netmask 255.255.255.0. Default: 127.0.0.1. Default netmask is 
255.255.255.255.

##### `-b <address>`
Bind HTTP server to this address. Default: listen on all interfaces.

##### `-j`
Disable JIT assembler if compiled scripts exist

##### `-v`
Be verbose. Can be specified two times for additional sound info.

##### `-q`
Be quiet / suppress warnings.
    
##### `-h`
Print a help text.
