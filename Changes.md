Feb 04 2023
===========
* Improvements in sound generation

This project has been suspended because EStim audio generation was re-implemented in JavaScript
and is now a part of the GAsm RTL (guide assembler runtime library). Thus, the MPGenerator
software and the GServer became obsolete. GAsm has been forked into a new project:
<https://gitlab.com/mantrid0/gasm>


Dec 23 2021
===========
* support of low duty cycle signals added

Dec 02 2021
============
* sound generation: improved phase modulation (less random)
* EstimLib: versions 1.10 and 1.11

Nov 28 2021
============
* sound generation:
    * new algorithm for calculation of modulation data (`MPSourceParam1`) replaced old ones
    * simplified configuration
* sound script syntax:
    * Parameter p_f added
* GASM/RTL:
    * function `setGotoRepeat` added
* EstimLib: versions 1.8 and 1.9
* clean-up of source tree (obsolete scripts removed, non-working scripts fixed)

Jul 4 2020
===========
* sound script syntax:
    * Parameter a_a2 now absolute
    * Parameter a_ar removed (was not used)
* gasm
    * Path completion for included scripts

Apr 16 2020
===========
* gserver / gasm / RTL:
    * stack tracing improved, Javascript errors logged to gserver
    * automatic server detection
    * Audio configuration menu
    * Automatic saving / restoring states
    * Infinite delays
    * Support of audio scripts with parameter and direct script loading vie RTL

Mar 27 2020
===========
* sound generation: improved volume normalization
* sound script syntax:
    * support for infinite sounds + abort
    * load command: include another script
* API improvements
    * Saving / loading configuration
    * MPAudioServer: Thread for audio playback and control
* EStimLib: version 1.7
* gasm: 
    * Assembler completed: File listings, Concatenating operator, smaller improvements
    * RTL: 
        * Manual saving / loading of the state
        * Support for gserver specific functions
        * Minor improvements
* gserver: First usable version of the guide server
	(see [gserver/Readme.md](https://gitlab.com/mantrid0/mpgenerator/blob/master/gserver/Readme.md))
    * All core functions but parameter handling for sound scripts implemented
    
    
Feb 20 2020
===========
* sound generation:
    * new algorithm for calculation of modulation data: MPSourceParam2
    * many optimizations of the old algorithm (MPSourceParam1)
    * generalizations of sound generation parameters (same parameter set for both algorithms)
    * simplified configuration (certain settings are chosen automatically)
* EStimLib: version 1.6
* gasm: First usable version
    * Assembler completed
    * RTL: all core functions but saving / restoring state implemented, documentation

Feb 02 2020
===========
* Changed range of pulse width modulation from 1..2 to 0.5..2
* EStimLib: version 1.5
* gasm: Guide Assembler (see [gasm/Readme.md](https://gitlab.com/mantrid0/mpgenerator/blob/master/gasm/Readme.md) )
    * Assembler implemented

Jan 26 2020
===========
* Frequency modulation replaced by pulse width modulation 
* Parameter set:
	* r.ah replaced by r.al, controls low frequency effects
	* pulse width modulation coupled t r.*
* EStimLib: version 1.4

Jan 05 2020
===========
* Parameter set: Frequency modulation now coupled to r.a
* EStimLib: version 1.3

Nov 27 2019
===========
* Frequency modulation supported in parameter set
* EStimLib: version 1.2a (w/o frequency modulation) and 1.2b (with frequency modulation)
* Examples updated
* Documentation updated

Nov 19 2019
===========
* Frequency modulation implemented

Oct 22 2019
===========
* Completely reworked audio generation
    * Old version: branch 'MPGenerator1'
    * Sinusoidal modulation frequency now fixed per channel
    * Phase modulation now independent from sinusoidal modulation (i.e. parameter 'p.f' replaces 'r.f')
    * Improved electrode setup (more simple and predictable)
    * A-waveform now independent per group (parameter 'a.s' removed).
    * Max-mixing (multiple time windows) removed
* EStimLib: version 1.1
* Documentation updated

Jun 05 2019
===========
* Bug in `MPSinkOggenc.java` fixed
* EStimLib: version 1.0

May 18 2019
===========
* Script language:
    * Bugfixes
    * new operator: rnd
    * new command: repeat/endrepeat
* Parameter set:
	* Rectangular modulation added (B-waveform)
	* More fine control for A-waveform
	* variation of sinusoidal waveform removed (use repeat/endrepeat instead)
* Sound file library started

Apr 10 2019
===========
* Phase sensitivity parameter
* `MPConverter`: Modulation phase extrapolation: now uses same parameteras as `MPGenerator`
* `MPConverter`, `TPConverter`: Shock and ramp modulation

Feb 08 2019
===========
* API improvements
* `MPConverter`: amplitude rescaling bug fixed

Jan 29 2019
===========
* API improvements
* Improved signal calculations
* Support of arithmetic expressions, user variables and positional parameters

Jan 25 2019
===========
* API improvements
* max-mix (instead of additive mix) allows predictable multi-channel combinations 
* carrier waveform not selectable anymore 
* continuous play
* oggenc support
* `MPGenerator`:
	* sinusoidal modulation improved, intensity parameter `-i` (use `-i 0,0,...` to reproduce old behavior)
	* pulse settings changed and not marked as experimental anymore
* `MPSimulator`
	* choosable channel factors (replaces -i)
* `MPConverter` (new): Converts 3-phase audio files into multi-phase audio files
* `TPConverter` (new): Converts 3-phase audio files into into power saving 3-phase files (only changes the carrier waveform).
