#!/bin/bash

set -e

DIR=mps
digits="0 1 2 3 4 5 6 7 8 9 x"

install -d $DIR

gen () {
    echo "load ${@:2}" > $DIR/$1.mps
}

. calib.sh
. normal.sh
. var.sh
. strokes.sh
. shocks.sh

cp *.mps $DIR
