# Shocks in sequences. A-waveform is used to for volume modulation and pause generation.
#
# $1 number of sequences
# 
# $2 HF effects
# $3 LF effects
#
# $4 volume at start of sequence
# $5 volume at end of sequence
#
# $6 min. total duration of shock sequence
# $7 max. total duration of shock sequence
#
# $8 min. total pause between shock sequence
# $9 max. total pause between shock sequence
#
# $10 min. number of sub-sequences
# $11 min. number of sub-sequences
#
# $12 volume increase relative to $4


@n = rnd($1 max 1)
@m1 = rnd($10 max 1)
@m2 = rnd($11 max 1)

# periodic waveform
r = $2
l = $3

p = 2
p_T = 10/(0.6+2.4*ran)

# run loop
@tmax = $6+$8
maxrun @tmax
@t = 0
repeat @n
    @m = rnd(@m1 + rnd(ran)*(@m2-@m1))
    @dt0 = ($8+ran*($9-$8))/@n
    @dt1 = ($6+ran*($7-$6))/@n
    @dv = @t/@tmax*$4*$12
    @t = @t + @dt0 + @dt1
    repeat @m
	# 50% pause
	a.a0 = 0
	a.a1 = 0
	a.a2 = 0
	run 0.5*@dt0/@m

	# shock sequnce
	a.a0 = $4 + @dv
	a.a1 = a.a0
	a.a2 = a.a0
	run 0
	a.a0 = $5 + @dv
	a.a1 = a.a0
	a.a2 = a.a0
	run @dt1/@m

	# 50% pause
	a.a0 = 0
	a.a1 = 0
	a.a2 = 0
	run 0
	run 0.5*@dt0/@m
    endrepeat
endrepeat
