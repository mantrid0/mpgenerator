# Shocks in sequences. A-waveform is used to for volume modulation and pause generation. B-waveform is used for the shocks.
#
# $1 number of sequences
# 
# $2 shock high time  
#
# $3 HF effects 
# $4 phase modulation
#
# $5 volume at start of sequence
# $6 volume at end of sequence
#
# $7 min. total duration of shock sequence
# $8 max. total duration of shock sequence
#
# $9 min. total pause between shock sequence
# $10 max. total pause between shock sequence
#
# $11 min. number of sub-sequences
# $12 max. number of sub-sequences
#
# $13 volume increase relative to $5


@n = rnd($1 max 1)
@m1 = rnd($11 max 1)
@m2 = rnd($12 max 1)

# B-waveform 
b.a = 1
b.t0 = 75 +  500*$2
b.t1 = 25 + 1000*$2
b.s = 0

r = $3
l = 1

p = $4
p_T = 10/(0.6+2.4*ran)

# run loop
@tmax = $7+$9
maxrun @tmax
@t = 0
repeat @n
    @m = rnd(@m1 + rnd(ran)*(@m2-@m1))
    @dt0 = ($9+ran*($10-$9))/@n
    @dt1 = ($7+ran*($8-$7))/@n
    @dv = @t/@tmax*$5*$13
    @t = @t + @dt0 + @dt1
    repeat @m
	# 50% pause
	a.a1 = 0
	a.a0 = 0
	a.a2 = 0
	run 0.5*@dt0/@m

	# shock sequnce
	a.a0 = $5 + @dv
	a.a1 = a.a0
	a.a2 = a.a0
	run 0
	a.a0 = $6 + @dv
	a.a1 = a.a0
	a.a2 = a.a0
	run @dt1/@m

	# 50% pause
	a.a0 = 0
	a.a1 = 0
	a.a2 = 0
	run 0
	run 0.5*@dt0/@m
    endrepeat
endrepeat
