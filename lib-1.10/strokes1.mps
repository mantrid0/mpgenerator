# More or less soft strokes in sequences
#
# $1 duration in s
#
# $2 volume at start
# $3 volume at end
#
# $4 HF effects at start
# $5 HF effects at at end
#
# $6 phase modulation at start
# $7 phase modulation at end
#
# $8  rise time
# $9  max. sequence time
# $10 avg. pause between sequences
# $11 max. number of strokes per sequence



# A-waveform / volume
a_a0 = 0
a_a1 = $2
a_a2 = 0
a_n = rnd($11)
a_tr = $8
a_t0 = ($9/a_n-a_tr)*0.1 + 0.05
a_t1 = ($9/a_n-a_tr)*0.9
a_t2 = $10

# B-waveform
b_a = 0

# periodic waveform
r = $4

# phase modulation
p = $6

run 1

a_a1 = $3

r = $5

p = $7
p_T = 10/(0.6+2.4*ran)

run $1-1
