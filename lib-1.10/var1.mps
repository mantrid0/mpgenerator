# Variations of: 
#   * HF effects (setting r)
#   * LF effects (setting l)
#   * phase modulation (setting p)
# With random and soft A-waveform modulation and B-waveform for overdrive shocks.
#
# $1 duration in s
# $2 number of variations
#
# $3 volume at start
# $4 volume at end
# $5 volume reset factor per variation
#
# $6  HF amplitude modulation at start
# $7  HF amplitude modulation at end
# $8  HF amplitude modulation  reset factor per variation
#
# $9  LF effects at start
# $10 LF effects at end
# $11 LF effects reset factor per variation
#
# $12 shock parameter (0..1) at start
# $13 shock parameter (0..1) at end
# $14 shock parameter reset factor per variation
# $15 shock parameter synchronization
#
# $16 A-waveform modulation
#
# $17 phase modulation

@n=rnd($2 max 1) 

# A-waveform / volume
@rvol = $5
@dvol = ($4-$3)/(@n-@n*@rvol+@rvol)
@vol = $3 + @dvol*@rvol
@aa = $16
@ta = 0.75+0.5*ran
a_t0 = 0
a_n = 4
a_t2 = 0.5*(a_n+1)*@ta*(1+ran)


# HF effects
@rr = $8
@dr = ($7-$6)/(@n-@n*@rr+@rr)
r = $6 + @dr*@rr

# LF effects
@rl = $11
@dl = ($10-$9)/(@n-@n*@rl+@rl)
l = $9 + @dl*l

# rectangular waveform / shock parameter
@rsp = $14
@dsp = ($13-$12)/(@n-@n*@rsp+@rsp)
@sp = $12 + @dsp*@rsp
b_s = $15

p = $17
p_T = 10/(0.6+2.4*ran)

# run loop
@vt = $1/@n
repeat @n
    # reset
    @vol = @vol - @dvol*@rvol
    a_tr = @ta/(1-@aa/2)*(1-ran*0.6)
    a_t1 = @ta-(1-@aa/2)*a_tr
    @oa = (a_t1+a_tr)/@ta
    a_a0 = @vol*(1-@aa)
    a_a1 = @vol*@oa
    a_a2 = @vol

    r = r - @dr*@rr

    l = l - @dl*@rl
    
    @sp = @sp - @dsp*@rsp
    b_t0 = 100 + 300*ran
    b_t1 = b_t0*(2+@sp)/(1+@sp)
    b_a = -@sp*500/(b_t0+250)
    
    run 1

    # variation step 
    @vol = @vol + @dvol
    a_a0 = @vol*(1-@aa)
    a_a1 = @vol*@oa
    a_a2 = @vol

    r = r + @dr

    l = l + @dl

    @sp = @sp + @dsp
    b_t1 = b_t0*(2+@sp)/(1+@sp)
    b_a = -@sp*500/(b_t0+250)
    
    run @vt - 1
    
endrepeat
