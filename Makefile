.PHONY: all clean distclean

JARTARGETS=MPGenerator.jar GAsm.jar GServer.jar

JAVAC=$(JAVAC_PATH)javac -Xlint:deprecation
JAR=$(JAVAC_PATH)jar
CLASSPATH=$(PKGDIR):.

all : $(JARTARGETS)

MPGenerator.jar: $(wildcard mpgenerator/*.java)
	@echo $^ $*
	$(JAVAC) $^
	$(JAR) cef mpgenerator.MPGenerator $@ mpgenerator/*.class

GAsm.jar: $(wildcard gasm/*.java)
	@echo $^
	$(JAVAC) $^
	$(JAR) cef gasm.GAsm $@ gasm/*.class

GServer.jar: $(wildcard gserver/*.java) $(wildcard gasm/*.java) $(wildcard mpgenerator/*.java)
	@echo $^
	$(JAVAC) $^
	$(JAR) cef gserver.GServer $@ gserver/*.class gasm/*.class mpgenerator/*.class

clean:
	rm -f */*.class 
	rm -f examples/*.wav tutorials/*.wav examples/*.ogg tutorials/*.ogg
	rm -f *.wav *.ogg
    
distclean: clean
	rm -f *.jar


