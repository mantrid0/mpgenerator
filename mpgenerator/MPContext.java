package mpgenerator;

import java.io.*;
import java.util.*;
import java.text.*;

// *****************************************************************************
// ******* MPContext ***********************************************************
// *****************************************************************************
/** Class which manages the configuration, signal settings, user variables and positional parameters.
  * Furthermore the memory functionality, script functions including the scrip interpreter are implemented.
  */
public class MPContext {
// configuration names
/** Carrier frequency in Hz. Default: 1000 */
    public final static String confNameCarrierFrequency = "carrierFrequency";
/** Duty ratio. Valid 0.1..1. Default: 1 */
    public final static String confNameDutyRatio = "dutyRatio";
/** HF Amplitude modulation frequency. This should be a slightly above the recognition limit. Default: "40" */
    public final static String confNameHighFrequency = "highFrequency";
/** Volume compensation. Default "0.5". Valid 0..1. */
    public final static String confNameVolumeCompensation = "volumeCompensation";
/** Channels per group as comma separated list. Default: "2" */
    public final static String confNameChannels = "channels";
/** Phase offsets per group. Comma separated list. Default: "0.5". Clipped to valid range of 0..1 */
    public final static String confNameOffsets = "offsets";
/** Sound generation algorithm. Currently supported: 1, 2 and 3. Default: "1" */
    public final static String confNameAlgo = "algo";
    
/** List of all configurationString */
    public final static String[] confNames = { confNameCarrierFrequency, confNameDutyRatio, confNameHighFrequency, confNameVolumeCompensation, confNameChannels, confNameOffsets, confNameAlgo };

    // signal settings and user variables
    private MPSettings settings = new MPSettings();				// signal settings
    private HashMap<String,Double> userVars = new HashMap<String,Double>();	// user variables
    private ArrayList<String> userVarsInit = new ArrayList<String>();		// initialized user variables, used to generate warnings
    
    // phase data source
    private MPSourceParam phaseSource = null;					// phase data source

    // properties variables and directories
    private char dirSep = '/';
    private String newLine = "\n";
    private String configDir = null;
    private String configFile = null;
    private String configName = "default";

    // configuration variables
/** Be verbose. Default: <code>true</code>. Not saved in configuration file. */
    public boolean verbose = true;
/** Carrier frequency in Hz: 1000. */
    private double confCarrierFrequency = 1000;
/** Duty ratio */
    private double confDutyRatio = 1;
/** HF Amplitude modulation frequency. This should be a slightly above the recognition limit. */
    private double confHighFrequency = 40;
/** Volume compensation. Default 0.5. Valid 0..1. */
    private double confVolumeCompensation = 0.5;
/** Channels per group. Default: { 2 } */
    private int[] confChannels = { 2 };
/** Phase offsets per group. Default: { 0.5 } */
    private double[] confOffsets = { 0.5 };
/** Sound generation algorithm: Currently supported: 1 and 2. Default: 1.*/
    private int confAlgo = 1;

/** Constructs a new instance without configuration file or user data directory.
  */
    public MPContext ( ) {
	dirSep = File.separatorChar;
	newLine = System.lineSeparator();
    }

  
/** 
  * Constructs a new instance with given configuration name. Configuration and user data is 
  * stored in <code>System.getProperty("user.home")+File.seperatorChar()+"MPGenerator"+File.seperatorChar()+confName</code>.
  * The directory is created if it does not exist.
  * @param confName Configuration name. If it is null, an empty string or "-" the value "default" is used.
  * @throws IOException If the directory does not exist and cannot be created.
  */  
    public MPContext ( String confName ) throws IOException, IllegalArgumentException {
	dirSep = File.separatorChar;
	newLine = System.lineSeparator();
	setConfigName(confName);
    }

/** 
  * Sets configuration and user data directory to <code>System.getProperty("user.home")+File.seperatorChar()+"MPGenerator"+File.seperatorChar()+confName</code>.
  * The directory is created if it does not exist.
  * @param confName Name if the configuration set. If it is null, an empty string or "-" the value "default" is used.
  * @throws IOException If the directory does not exist and cannot be created or if an error occurred while attempting to read the configuration file.
  * @throws IllegalArgumentException If a syntax error occurred in the configuration file.
  * @return Return true if a configuration file has been read. (Missing configuration file is not considered as an error.)
  */  
    public boolean setConfigName( String confName ) throws IOException, IllegalArgumentException {
	if ( (confName==null) || (confName.length()==0) || confName.equals("-") ) confName = "default";
	
	configDir = System.getProperty("user.home")+dirSep+".MPGenerator"+dirSep+confName;
	configFile = configDir + dirSep + "config";
	try {
	    File f = new File(configDir);
	    if ( (!f.canWrite()) && (!f.mkdirs()) ) throw new Exception();
	    if ( !f.isDirectory() ) throw new Exception();
	    configName = confName;
	} catch ( Exception e ) {
	    throw new IOException("Unable tor create or to write to `"+configDir+"`");
	}
	return readConfigFile();
    }

/** 
  * Gets the configuration name.
  * @return Returns the configuration name.
  */  
    public String configName() {
	return configName;
    }

/** 
  * Gets the configuration directory.
  * @return Returns the configuration directory.
  */  
    public String configDir() {
	return configDir;
    }

/** 
  * Gets the path of the configuration file.
  * @return Returns the path of the configuration file
  */  
    public String configFile() {
	return configFile;
    }

    
// ******* evaluation of arithmetic expression *********************************
    // tokens types
    private static enum TT {
	// special
	open,	// (
	// unary
	abs,
	sqr,
	sqrt,
	sin,
	cos,
	rnd,
	neg,
	// binary
	min,
	max,
	pow,	// ^
	div,	// /
	mul,	// *
	sub,	// -
	add,	// +
    }
    private static final int tt_first_op = TT.abs.ordinal();
    private static final int tt_first_binary = TT.min.ordinal();
    
    // token in a list
    static private class Token {
	TT tt;
	double val;
	Token prev,next;
	
	Token ( TT tt, double val, Token prev, Token next ) {
	    this.tt = tt;
	    this.val = val;
	    this.prev = prev;
	    if ( prev!=null ) prev.next = this;
	    this.next = next;
	    if ( next!=null ) next.prev = this;
	}
    }
    
    // token list
    static private class TokenList {
	Token first = null,last = null;
	
	void push ( double val ) {
	    last = new Token(null, val, last, null);
	    if ( first==null ) first = last;
	}

	void push ( TT tt ) {
	    last = new Token(tt, 0, last, null);
	    if ( first==null ) first = last;
	}
	
	TT pop () {
	    if ( last == null ) return null;
	    TT tt = last.tt;
	    last = last.prev;
	    if ( last == null ) first=null;
	    return tt;
	}

	TT last () {
	    return ( last==null ) ? null : last.tt;
	}
	
	double eval () throws ParseException {
	    if ( last==null ) throw new ParseException("Invalid expression",0);
	    TT tt = last.tt;
	    Double v = last.val;
	    last = last.prev;
	    if ( last == null ) first=null;
	    
	    if ( tt==null ) return v;
	    else if ( tt==TT.abs ) return Math.abs(eval());
	    else if ( tt==TT.sqr ) { 
		double r=eval(); return r*r; 
	    }
	    else if ( tt==TT.sqrt ) return Math.sqrt(Math.max(0, eval()));
	    else if ( tt==TT.sin ) return Math.sin(eval());
	    else if ( tt==TT.cos ) return Math.cos(eval());
	    else if ( tt==TT.rnd ) return Math.round(eval());
	    else if ( tt==TT.neg ) return -eval();
	    else if ( tt==TT.min ) return Math.min(eval(),eval());
	    else if ( tt==TT.max ) return Math.max(eval(),eval());
	    else if ( tt==TT.pow ) { 
		double b=eval(), a=eval(); 
		if ( Math.abs(a)<1e-100 ) return 0;
		b = Math.exp(Math.log(Math.abs(a))*b);
		return ( a<0 ) ? -b : b;
	    }
	    else if ( tt==TT.mul ) return eval()*eval();
	    else if ( tt==TT.div ) { 
		double b=eval(), a=eval(); 
		return a/b;
	    }
	    else if ( tt==TT.add ) return eval()+eval();
	    else if ( tt==TT.sub ) { 
		double b=eval(), a=eval(); 
		return a-b;
	    }
	    else throw new ParseException("Invalid expression",0);
	}
	
	void print() {
	    Token t = first;
	    while ( t != null ) {
		if ( t.tt == null ) System.out.print(t.val+" ");
		else System.out.print(t.tt.name()+" ");
		t = t.next;
	    }
	}
    }
    
/** 
  * Evaluates an arithmetic expression.
  * Three types of variables are supported:
  * <ul>
  * <li> Parameters for signal generation, see {@link MPSettings.S}
  * <li> User variables that start with <code>@</code> are supplied by parameter <code>vars</code>.
  * <li> Positional parameters of the form <code>$&lt;n&gt;</code>. Their valid range is -1 to 1 and they have a default value of 0. First positional parameter is <code>$1</code>.
  * </ul>
  * Valid characters for parameters and user variables are digits, Latin letters, '_' and '.'. Case is ignored.
  * These unary operators are supported. Unary operators have the maximal precedence (strongest binding).
  * <table>
  * <caption>Unary operators</caption>
  * <tr><th>Operator</th><th>Description</th></tr>
  * <tr><td>abs</td><td>Absolute value</td></tr>
  * <tr><td>sqr</td><td>Square value, also see <code>^</code> operator</td></tr>
  * <tr><td>sqrt</td><td>Square root or 0 if argument is negative, also see <code>^</code> operator</td></tr>
  * <tr><td>sin</td><td>Sine of an angle in radians</td></tr>
  * <tr><td>cos</td><td>Cosine of an angle in radians</td></tr>
  * <tr><td>rnd</td><td>Value rounded to nearest integer</td></tr>
  * </table>
  * These binary operators are supported. Operators with higher precedence (stronger binding) are stated first.
  * <table>
  * <caption>Binary operators</caption>
  * <tr><th>Operator</th><th>Description</th></tr>
  * <tr><td><code>min</code></td><td>Result is the minimum or the operands</td></tr>
  * <tr><td><code>max</code></td><td>Result is the maximum or the operands</td></tr>
  * <tr><td><code>^</code></td><td><code>a^b</code> Defined as <code>a<sup>b</sup></code> if <code>a</code> is greater than zero, <code>0</code> if <code>a</code> if equal to zero and <code>-(-a)<sup>b</sup></code> if <code>a</code> is less than zero. Thus, <code>a^0</code> is the sign function.</td></tr>
  * <tr><td><code>*</code></td><td>Multiplication</td></tr>
  * <tr><td><code>/</code></td><td>Division</td></tr>
  * <tr><td><code>+</code></td><td>Addition</td></tr>
  * <tr><td><code>-</code></td><td>Subtraction</td></tr>
  * </table>
  * These internal variables and constants are supported:
  * <table>
  * <caption>Variables and Constants</caption>
  * <tr><th>Variable</th><th>Description</th></tr>
  * <tr><td><code>ran</code></td><td>Random value greater or equal zero and smaller than one.</td></tr>
  * <tr><td><code>pi</code></td><td>&pi;</td></tr>
  * </table>
  * @param expr Expression to evaluate 
  * @param settings Parameters for signal generation. If null, these variables are not evaluated.
  * @param vars User Variables. If null, these variables are not evaluated.
  * @param varsInit List of initialized user Variables. Only used to generate warnings If null, this feature is disabled.
  * @param params Positional parameters.  Can be null if no positional parameters are defined.
  * @return The result of the expression.
  * @throws ParseException If a syntax error occurred.
  */
    public static double eval ( String expr, MPSettings settings, Map<String,Double> vars, List<String> varsInit, double[] params ) throws ParseException {
	TokenList ops = new TokenList();
	TokenList out = new TokenList();
	expr = expr.toLowerCase(Locale.US);

	int i=0, j;
	double val;
	boolean prev_op = true;		// previous token was an operator
	boolean prev_par = false; 	// previous token was a parenthesis
	boolean val_req = true; 	// value required
	while ( i<expr.length() ) {
	    while ( (i<expr.length()) && (expr.charAt(i)<=' ') )
		i++;
	    if ( i>=expr.length() ) break;
	    char ch = expr.charAt(i);
	    j=i+1;

	    // read token
	    TT tt = null;
	    val=0;	    
	    boolean close = false;
	    if ( ch=='$' ) {
		j=i+1;
		while ( (j<expr.length()) && (expr.charAt(j)>='0') && (expr.charAt(j)<='9') ) 
		    j++;
		int k;
		try {
		    if ( j-i<2 ) throw new Exception();
		    k = Integer.valueOf(expr.substring(i+1,j));
		    if ( k<1 ) throw new Exception();
		} catch ( Exception e ) {
		    throw new ParseException("Invalid positional parameter (first positional parameter is $1",i);
		}
		val = ( (params!=null) && (k<=params.length) ) ? params[k-1] : 0;
	    }
	    else if ( ch=='@' ) {
		j=i+1;
		while ( (j<expr.length()) && ( (expr.charAt(j)=='.') || (expr.charAt(j)=='_') || ((expr.charAt(j)>='0') && (expr.charAt(j)<='9')) || ((expr.charAt(j)>='a') && (expr.charAt(j)<='z')) ) ) 
		    j++;
		if ( j-i<2 ) throw new ParseException("Invalid user variable",i);
		String ss = expr.substring(i+1,j);
		Double d = (vars==null) ? null : vars.get(ss);
		if ( d==null ) throw new ParseException("Undefined user variable",i);
		if ( varsInit!=null && !varsInit.contains(ss) ) MPLogger.err.println("Warning: user variable @"+ss+" may not be initialized correctly.");
		val = d;
	    }
	    else if ( ch=='+' ) tt=TT.add;
	    else if ( ch=='*' ) tt=TT.mul;
	    else if ( ch=='/' ) tt=TT.div;
	    else if ( ch=='^' ) tt=TT.pow;
	    else if ( ch=='(' ) tt=TT.open;
	    else if ( ch==')' ) close=true;
	    else if ( /*((ch=='-') && (prev_op || prev_par)) || */ ((ch>='0') && (ch<='9')) ) {  // token is a number
		while ( (j<expr.length()) && ( (expr.charAt(j)=='.') || ((expr.charAt(j)>='0') && (expr.charAt(j)<='9')) || ((expr.charAt(j)>='a') && (expr.charAt(j)<='z')) ) ) 
		    j++;
		try {
		    val = Double.valueOf(expr.substring(i,j));
		} catch ( Exception e ) {
		    throw new ParseException("Not a number",i);
		}
	    } else if ( (ch>='a') && (ch<='z') ) {	// operator or internal variable. Must start with a letter
		j=i+1;
		while ( (j<expr.length()) && ( (expr.charAt(j)=='.') || (expr.charAt(j)=='_') || ((expr.charAt(j)>='0') && (expr.charAt(j)<='9')) || ((expr.charAt(j)>='a') && (expr.charAt(j)<='z')) ) ) 
		    j++;
		String ss = expr.substring(i,j);
		// evaluate operators
		TT[] tts = TT.values();
		for ( int k=tt_first_op; (tt==null) && (k<tts.length); k++ )	
		    if ( ss.equals(tts[k].name() ) ) tt=tts[k];
		// evaluate settings variables
		if ( tt == null ) {
		    MPSettings s = settings;
		    if ( s!=null ) {
			try {
			    val = s.get(ss);
			} catch ( Exception e ) {
			    s = null;
			}
		    }
		    if ( s == null ) {
			if ( ss.equals("ran") ) val = Math.random();
			else if ( ss.equals("pi") ) val = Math.PI;
			else throw new ParseException("Operator or variable expected",i);
		    }
		}
	    }
	    else if ( ch=='-' ) tt = (prev_op || prev_par) ? TT.neg : TT.sub;
	    else throw new ParseException("Unexpected character",i);

	    if ( (tt==null) && (!close) && (!prev_op) ) throw new ParseException("Operator expected",i);
	    if ( (tt!=null) && (tt.ordinal()>tt_first_binary) && val_req ) throw new ParseException("Unexpected binary operator",i);
	    
	    // process token 
	    if ( close ) {
	        while ( (ops.last()!=null) && (ops.last()!=TT.open) )
		    out.push(ops.pop());
		if ( ops.last()==null ) {
		    throw new ParseException("`)` without `(`",i);
		}
		ops.pop();
	    }
	    else if ( tt==null ) out.push(val);
	    else {
		int o = tt.ordinal();
		if ( o<tt_first_binary ) {
		    ops.push(tt);
		}
		else {
		    while ( (ops.last()!=null) && (ops.last().ordinal()<=o) && (ops.last()!=TT.open) )
			out.push(ops.pop());
		    ops.push(tt);
		}
	    }
	    prev_par = (tt!=null) && ((tt==TT.open) || close); 
	    if ( !prev_par ) prev_op = (tt!=null) && (tt!=TT.open) && (!close); 
	    val_req = (tt!=null) && ((tt==TT.open) || (tt.ordinal()>=tt_first_binary));
	    i=j;
	}
	if ( val_req ) throw new ParseException("Value or variable expected", expr.length());
	while ( ops.last!=null ) {
	    if ( ops.last()==TT.open ) throw new ParseException("`(` without `)`",-1);
	    out.push(ops.pop());
	}
//	System.out.print(expr+": ");
//	out.print();
	val = out.eval();
//	System.out.print(" = "+val+"    ");
//	out.print();
//	System.out.println();
	if ( out.last!=null ) throw new ParseException("Invalid expression",0);
	return val;
    }
    
/** 
  * Evaluates a arithmetic expression, see {@link #eval(String,MPSettings,Map,List,double[])}.
  * @param expr The expression to evaluate.
  * @param params Positional parameters.
  * @return The result of the expression.
  * @throws ParseException If a syntax error occurred.
  */
    public double eval (String expr, double[] params ) throws ParseException {
	return eval(expr, settings, userVars, userVarsInit, params);
    }

/** 
  * Evaluates a arithmetic expression, see {@link #eval(String,MPSettings,Map,List,double[])}.
  * @param expr The expression to evaluate.
  * @return The result of the expression.
  * @throws IllegalArgumentException If a syntax error occurred.
  */
    public static double eval (String expr ) throws IllegalArgumentException {
	try {
	    return eval(expr, null, null, null, null);
	} catch ( ParseException pe ) {
	    int offs = pe.getErrorOffset();
	    if ( offs >=0 ) 
	    throw new IllegalArgumentException("Error parsing expression \""+expr+ ( (offs>=0) ? "\" at position " + offs + ": " : "\": " ) + pe.getLocalizedMessage() );
	}
	return 0; // makes the compiler happy
    }

/** 
  * Generates a nice error syntax error message.
  * @param pos Position string, e.g. a file name and a line number. Can be null.
  * @param msg Message string
  * @param expr The expression with the syntax error
  * @param offs Position of the Syntax error (starts with 0)
  * @return A nicely formatted message.
  */
    public static String getParseExceptionMessage(String pos,  String msg, String expr, int offs ) {
	StringBuilder sb = new StringBuilder();
	if ( pos!=null) sb.append(pos+": ");
	sb.append("Syntax error: " + msg + System.lineSeparator());
	sb.append("        " + expr );
	if ( (offs>=0) && (offs<=expr.length()) ) {
	    sb.append(System.lineSeparator() + "        " );
	    for ( int i=0; i<offs; i++ ) 
		sb.append(' ');
	    sb.append('^');
	}
	return sb.toString();
    }

/** 
  * Generates a nice error syntax error message.
  * @param pos Position String, e.g. a file name and a line number or a variable name. Can be null.
  * @param expr The expression with the syntax error
  * @param pe ParseException Exception that with message and error position.
  * @return A nicely formatted message.
  */
    public static String getParseExceptionMessage(String pos, String expr, ParseException pe ) {
	return getParseExceptionMessage(pos, pe.getLocalizedMessage(), expr, pe.getErrorOffset());
    }

// ******* variables and signal settings ***************************************
/** 
  * Get the signal settings for reading or writing.
  * @return The signal settings.
  */  
    public MPSettings getSignalSettings() {
	return settings;
    }
    
/** 
  * Set a user variable
  * @param n Name of the variable. Will be trimmed and converted to lower case. Leading '@''s are ignored.
  * @param v The value.
  * @throws ParseException If <code>n</code> contains illegal characters.
  */  
    public void setUserVar(String n, double v) throws ParseException {
	n = n.toLowerCase(Locale.US);
	// check whether variable name is valid
	int i = 0;
	while ( (i<=n.length()) && ((n.charAt(i)<=' ') || (n.charAt(i)=='@')) ) 
	    i++;
	int j = i;
	while ( (j<n.length()) && ( (n.charAt(j)=='.') || (n.charAt(j)=='_') || ((n.charAt(j)>='0') && (n.charAt(j)<='9')) || ((n.charAt(j)>='a') && (n.charAt(j)<='z')) ) ) 
	    j++;
	for (int k=j; k<n.length(); k++ ) 
	    if ( n.charAt(k)>' ' ) throw new ParseException("Invalid character in variable name",k);
	if ( j == i ) throw new ParseException("Empty variable name",j);
	// store value
	n = n.substring(i,j);
	userVars.put(n,v);
	if ( !userVarsInit.contains(n) ) userVarsInit.add(n);
    }

// ******* configurations ******************************************************
/** 
  * Reads the configuration file. 
  * @throws IOException If an error occurred while attempting to read the configuration file.
  * @throws IllegalArgumentException If a syntax error occurred.
  * @return Return true if the configuration file has been read. (Missing configuration file is not considered as an error.)
  */  
    private boolean readConfigFile( ) throws IOException, IllegalArgumentException {
	if ( configFile == null ) return false;
	File file = new File( configFile );
	if ( ! file.isFile() ) return false;

	Reader in = new FileReader(file);
	BufferedReader rd = new BufferedReader(in);
	int line = 0;
	while (true) {
	    line++;
	    String ls = rd.readLine();
	    if ( ls == null ) {
	        in.close();
	        rd.close();
	        return true;
	    }
	    ls = ls.trim();
	    if ( (ls.length()<1) || (ls.charAt(0)=='#') ) continue;
	    int si = ls.indexOf('=');
	    if ( si < 0 ) throw new IllegalArgumentException(configFile+" ("+line+"): `=' expected");
	    String msg = setConfig(ls.substring(0,si).trim(), ls.substring(si+1).trim());
	    if ( msg!=null ) throw new IllegalArgumentException(configFile+" ("+line+"): " + msg + " after '='");
	}
    }

    private void writeConfigFileEntry( PrintStream ps, String name, String comment) throws IOException {
	ps.println("# "+name);
	ps.println("# "+comment);
	ps.println(name + " = " + getConfig(name)+"\n");
    }

    
/** 
  * Writes the configuration file. 
  * @throws IOException I the configuration file cannot be written.
  * @return Return false if no configuration file has been defined.
  */  
    public boolean writeConfigFile() throws IOException {
	if ( configFile == null ) return false;
	try {
    	    PrintStream ps = new PrintStream(configFile);
    	    writeConfigFileEntry(ps, confNameCarrierFrequency, "Carrier frequency in Hz" );
    	    writeConfigFileEntry(ps, confNameDutyRatio, "Duty ratio" );
    	    writeConfigFileEntry(ps, confNameHighFrequency, "High frequency amplitude modulation frequency. This should be a slightly above the recognition limit." );
    	    writeConfigFileEntry(ps, confNameVolumeCompensation, "Volume compensation. Valid 0..1." );
    	    writeConfigFileEntry(ps, confNameChannels, "Channels per group as comma separated list");
    	    writeConfigFileEntry(ps, confNameOffsets, "Phase offsets per group as comma separated list");
    	    writeConfigFileEntry(ps, confNameAlgo, "Sound generation algorithm. Currently supported: 1.");
    	    ps.close();
    	}
    	catch ( Exception e ) {
    	    throw new IOException(e.getLocalizedMessage());
    	}
    	return true;
    }


/** 
  * Get configuration as string. 
  * @param name Name of configuration.
  * @return null If configuration variable does not exist
  */  
    public String getConfig(String name) {
	if ( name == null ) return null;
	if ( name.equalsIgnoreCase(confNameCarrierFrequency) ) {
	    return Double.toString(confCarrierFrequency);
	}
	else if ( name.equalsIgnoreCase(confNameDutyRatio) ) {
	    return Double.toString(confDutyRatio);
	} else if ( name.equalsIgnoreCase(confNameHighFrequency) ) {
	    return Double.toString(confHighFrequency);
	} else if ( name.equalsIgnoreCase(confNameVolumeCompensation) ) {
	    return Double.toString(confVolumeCompensation);
	} else if ( name.equalsIgnoreCase(confNameAlgo) ) {
	    return Integer.toString(confAlgo);
	} else if ( name.equalsIgnoreCase(confNameChannels) ) {
	    String s = "";
	    for ( int i=0; i<confChannels.length; i++ ) 
		s+= (i>0) ? ","+confChannels[i] : confChannels[i];
	    return s;
	} else if ( name.equalsIgnoreCase(confNameOffsets) ) {
	    String s = "";
	    for ( int i=0; i<confOffsets.length; i++ ) 
		s+= String.format(Locale.US, i>0 ? ",%.2f" : "%.2f", confOffsets[i]);
	    return s;
	} 
	return null;
    }

/**
  * Get names and values of all configurations as a map
  * @return Return the configurations as a map
  */
    public HashMap<String,String> getConfigs () {
	HashMap<String,String> m = new HashMap<String,String>();
	final String[] ns = confNames;
	for (int i=0; i<ns.length; i++ )
	    m.put(ns[i], getConfig(ns[i]));
	return m;
    }

/** 
  * Writes the configuration file. 
  * @param name Name of configuration.
  * @param val Value of configuration.
  * @return An error message or null if no errors occurred.
  */  
    public String setConfig(String name, String val) {
	if ( name.equalsIgnoreCase(confNameCarrierFrequency) ) {
	    try {
    		confCarrierFrequency = Double.parseDouble( val );
	    } 
	    catch (Exception e) {
		return "Number expected";
	    }
	} else if ( name.equalsIgnoreCase(confNameDutyRatio) ) {
	    try {
    		confDutyRatio = Double.parseDouble( val );
	    } 
	    catch (Exception e) {
		return "Number expected";
	    }
	} else if ( name.equalsIgnoreCase(confNameHighFrequency) ) {
	    try {
    		confHighFrequency = Double.parseDouble( val );
	    } 
	    catch (Exception e) {
		return "Number expected";
	    }
	} else if ( name.equalsIgnoreCase(confNameVolumeCompensation) ) {
	    try {
    		confVolumeCompensation = Double.parseDouble( val );
	    } 
	    catch (Exception e) {
		return "Number expected";
	    }
	} else if ( name.equalsIgnoreCase(confNameAlgo) ) {
	    try {
    		confAlgo = (int) Math.round(Double.parseDouble( val ));
	    } 
	    catch (Exception e) {
		return "Algorithm number expected";
	    }
	} else if ( name.equalsIgnoreCase(confNameChannels) ) {
	    try {
		String[] sa = val.split(",");
		int[] a = new int[sa.length];
		for (int j=0; j<sa.length; j++ ) 
		    a[j] = Integer.parseInt( sa[j] );
		confChannels = a;
	    } 
	    catch (Exception e) {
	        return "Comma separated list of channels per group expected";
	    }
	} else if ( name.equalsIgnoreCase(confNameOffsets) ) {
	    try {
		String[] sa = val.split(",");
		double[] a = new double[sa.length];
		for (int j=0; j<sa.length; j++ ) 
		    a[j] = Double.parseDouble( sa[j] );
		confOffsets = a;
	    } 
	    catch (Exception e) {
	        return "Comma separated list of phase offsets per group expected";
	    }
	} else return "Invalid configuration name: '" + name + "'";
	return null;
    }


/** 
  * Creates a {@link MPGenerator} instance from current configuration.
  * @return A new @link MPGenerator} instance.
  */  
    public MPGenerator getGenerator() {
	int l = 0;
	if ( confChannels != null )
	    for ( int i=0; i<confChannels.length; i++ )
		l+=Math.abs(confChannels[i]);
	if ( l < 0 ) l=0;
	return new MPGenerator1( l, confHighFrequency, confCarrierFrequency, confDutyRatio, verbose);
    }

// ******* script commands *****************************************************
/** 
  * Run command
  * @param gen The audio data generator.
  * @param sink The audio data sink.
  * @param runTime  The run time in s.
  * @throws IOException If the audio data cannot be processed for any reason.
  */  
    public synchronized void cmdRun (MPGenerator gen, MPSink sink, double runTime ) throws IOException {
        phaseSource.load( settings,runTime );
	gen.run(phaseSource, sink);
    }


// ******* script processing ***************************************************
/** 
  * Reads and executes a script from Reader. The difference between this method and {@link #readScript} is that this method does not reset the signal settings.
  * @param gen The audio data generator.
  * @param sink The audio data sink.
  * @param in The Reader instance which reads the script
  * @param sname Name of the script used for generating error messages. If null, it will be replaced by "(anonymous)".
  * @param dir An optional search directory for included scripts (can be null).
  * @param posParams Positional parameters.
  * @throws IOException if an error occurs while attempting to read the script.
  * @throws IllegalArgumentException If a syntax error occurred.
  */  
    protected synchronized void includeScript( MPGenerator gen, MPSink sink, Reader in, String sname, String dir, double[] posParams) throws IllegalArgumentException, IOException {
	final int rptLMax = 32;
	int rptL = -1;  			// repeat level 
	int[] rptLine = new int[rptLMax];	// position if repeat commend
	int[] rptCount = new int[rptLMax];	// number of remaining repeats

	
	int line = 0;
	String ls = "";
	BufferedReader rd = new BufferedReader(in);
	HashMap<Integer,String> lineBuf = new HashMap<Integer,String>();
	try {
	    while ( (phaseSource.getMaxTime()!=0) && (sink.isRunning()) ) {
		line++;
		if ( lineBuf.containsKey(line) ) {
		    ls = lineBuf.get(line);
		} else {
		    ls = rd.readLine();
	            if ( ls == null ) {
		        in.close();
			rd.close();
			return;
		    }
		    lineBuf.put(line,ls);
		}
		int i=0;
		while ((i<ls.length()) && (ls.charAt(i)<=' ')) 
		    i++;
		if ( ( i>=ls.length()) || (ls.charAt(i)=='#') ) continue;
		int j = ls.indexOf('=');
		if ( j > 0 ) { // assignment
		    Double val;
		    try {
		        val = eval(ls.substring(j+1), posParams);
		    } catch ( ParseException pe) {
		        throw new ParseException(pe.getLocalizedMessage(), pe.getErrorOffset()+j+1);
		    }
		    String name = ls.substring(i,j);
		    if ( name.charAt(0)=='@' ) {	// user variable
		        try {
		    	    setUserVar(name,val);
			} catch ( ParseException pe) {
		    	    throw new ParseException(pe.getLocalizedMessage(), pe.getErrorOffset()+i);
			}
		    } else {				// parameter
			try {
			    settings.set(name.trim(),val);
			} catch (Exception e) {
		    	    throw new ParseException(e.getLocalizedMessage(), i);
		    	}
		    }
		} else {	// commands 
		    String[] args = ls.trim().split("\\s",2);
		    String name = args[0].toLowerCase(Locale.US);
		    String params = (args.length>1) ? args[1] : null;
		    if ( name.equals("run") ) {
			double T;
			ls = params;	// for parser error message
			T = eval( params, posParams );
//    			System.out.println("Generating "+T+" s of sound data");
			cmdRun(gen,sink, T);
		    } else if ( name.equals("load") ) {
			if ( params == null ) throw new ParseException("Parameter expected after load",i);
			args = params.trim().split("\\s"); // first argument is always non-empty
			File file = null;
			if ( dir != null ) {
			    file = new File(dir,args[0]);
			    if ( ! file.isFile() ) file = null;
			}
			if ( file == null ) file = new File(args[0]);
			if (! file.isFile() ) {
			    ls = args[0];	// for parser error message
			    throw new ParseException("File not found",0);
			}
			j=0;
			for ( int k=1; k<args.length; k++ )
			    if ( (args[k]!=null) && (args[k].length()>0) ) j++;
			double[] pp = (j>0) ? new double[j] : null;
			j=0;
			for ( int k=1; k<args.length; k++ ) {
			    if ( (args[k]!=null) && (args[k].length()>0) ) {
				ls = args[k];
				pp[j] = eval( ls, posParams );
				j++;
			    }
			}
			includeScript(gen, sink, file.getPath(), pp);
		    } else if ( name.equals("maxrun") ) {
			double T;
			ls = params;	// for parser error message
			T = eval( params, posParams );
			phaseSource.setMaxTime(T);
		    } else if ( name.equals("repeat") ) {
    			rptL++;
			if ( rptL>=rptLMax ) throw new ParseException("Maximum number of repeat levels exceeded",i);
			int k;
			try {
			    k = (int) Math.round(eval( params, posParams ));
			} catch ( ParseException pe) {
			    ls = params;	// for parser error message
			    throw pe;
			}
			if ( (k<1) && (rptL>0) ) throw new ParseException("Minimum number of inner repeats is 1",i);
			rptLine[rptL] = line;
			rptCount[rptL] = k-1;
		    } else if ( name.equals("endrepeat") ) {
			if ( rptL < 0 ) throw new ParseException("endrepeat without repeat",i);
			if ( /*(phaseSource.getMaxTime()!=0) && (sink.isRunning()) &&*/ (rptCount[rptL]!=0) ) {
			    line = rptLine[rptL];
			    if ( rptCount[rptL]>0 ) rptCount[rptL]--;
			} else {
			    rptL--;
			}
		    } else {
			throw new ParseException("Invalid command or syntax error: "+ls, i);
		    }
		}
	    }
	} catch ( ParseException pe ) {
	    in.close();
	    rd.close();
	    throw new IllegalArgumentException(getParseExceptionMessage( ((sname==null) ? "(anonymous)" : sname) + " ("+line+")", ls, pe));
	}
    }

/** 
  * Reads and executes a script from file. The difference between this method and {@link #readScript} is that this method does not reset the signal settings.
  * @param gen The audio data generator.
  * @param sink The audio data sink.
  * @param fn Filename of the script
  * @param posParams Positional parameters.
  * @throws IOException if an error occurs while attempting to read the file.
  * @throws IllegalArgumentException If a syntax error occurred.
  */  
    protected void includeScript (MPGenerator gen, MPSink sink, String fn, double[] posParams) throws IllegalArgumentException, IOException {
	String dir = new File(fn).getParent();
	Reader in = new FileReader(fn);
	includeScript(gen, sink, in, fn, dir, posParams);
    }


// Returns the phase data source based on current settings
// v: verbose
    private MPSourceParam getSource(boolean v) throws IllegalArgumentException {
	if ( confAlgo == 1 ) return new MPSourceParam1( confHighFrequency, confVolumeCompensation, confChannels, confOffsets, v );
	throw new IllegalArgumentException("Invalid algorithm: "+confAlgo);
    }

/** 
  * Tests the configuration
  * @return null if configuration if valid, otherwise an error message.
  */  
    public String testConfig() {
	try {
	    getSource(false);
	}
	catch ( Exception e ) {
	    return e.getLocalizedMessage();
	}
	return null;
    }


/** 
  * Reads and executes a script from file. Signal settings are reset and user variables are preserved. 
  * @param gen The audio data generator.
  * @param sink The audio data sink.
  * @param fn Filename of the script
  * @param posParams Positional parameters. Optional, can bu null.
  * @throws IOException if an error occurs while attempting to read the file
  * @throws IllegalArgumentException if a syntax or configuration error occurred.
  */  
    public synchronized void readScript (MPGenerator gen, MPSink sink, String fn, double[] posParams) throws IllegalArgumentException, IOException {
	settings.reset();
	phaseSource = getSource(verbose);
	includeScript(gen, sink, fn, posParams );
    }

/** 
  * Reads and executes a script from Reader. Signal settings are reset and user variables are preserved. 
  * @param gen The audio data generator.
  * @param sink The audio data sink.
  * @param in The Reader instance which reads the script
  * @param sname Name of the script used for generating error messages. If null, it will be replaced by "(anonymous)".
  * @param dir An optional search directory for included scripts (can be null).
  * @param posParams Positional parameters. Optional, can bu null.
  * @throws IOException if an error occurs while attempting to read the file
  * @throws IllegalArgumentException if a syntax or configuration error occurred.
  */  
    public synchronized void readScript (MPGenerator gen, MPSink sink, Reader in, String sname, String dir, double[] posParams) throws IllegalArgumentException, IOException {
	settings.reset();
	phaseSource = getSource(verbose);
	includeScript(gen, sink, in, sname, dir, posParams );
    }
    
    
/** 
  * Marks all user variables an uninitialized.
  */
    public void userVarsUnInit() {
	userVarsInit.clear();
    }


/** 
  * Internal tests.
  * @param args Command line parameters
  */  
    public static void main(final String[] args) {
	String[] exprs = { 
	    "-1",
	    "1+-2",
	    "1-2/3",
	    "(1+2)*3",
	    "1/2*3",
	    "1-2+3",
	    "1-2-3",
	    "1/2/3",
	    "sin abs -1*pi",
	    "cos(abs -1*pi)",
	    "2 min 1 min 3",
	    "2 max 1 max 3",
	    "a_a1*r.f",
	    "sqrt(sqr(1.234))",
	    "sqrt(-1)",
	    "-2^3",
	    "@abc*($1+$5)",
	    "ran",
	    "rnd ran",
	    "rnd ran",
	    "rnd ran",
	    "rnd ran",
	    "-ran",
	    "--ran",
	    "--ran-1",
	};
	
	int i=0;
	MPContext context = new MPContext();
	try {
	    context.setUserVar("ABC",1.2);
	    for ( i=0; i<exprs.length; i++ )
		System.out.println(exprs[i] + " = "+ context.eval(exprs[i], new double[] { 1,2 }) );
	} catch ( ParseException pe ) {
	    System.out.println(getParseExceptionMessage(null, pe.getLocalizedMessage(), exprs[i], pe.getErrorOffset() ));
	}
    }
}
