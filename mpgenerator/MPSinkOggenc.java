package mpgenerator;

import java.io.*;
import java.util.*;
import java.text.*;

import javax.sound.sampled.*;

// *****************************************************************************
// ******* MPSinkOggenc ********************************************************
// *****************************************************************************
/** Class that writes audio data to an .ogg file using <code>oggenc</code>.
  * The external program <code>oggenc</code> must be installed in one of the directories specified by the 
  * <code>PATH</code> environment variable.
  */
public class MPSinkOggenc implements MPSink {
    private int channels;
    private float f_frames;
    private boolean verbose;
    private String fileName;
    private String oggencPath = null;	// absolute path of oggenc
    private Process oggencProc = null;
    
/** Constructor. In most cases {@link MPGenerator#getMPSink(String)} should be used for creating an instance.
  * @param cn Number of channels
  * @param f_frames Frame rate
  * @param verbose Be verbose.
  * @param fn Name off the .ogg file. 
  * @throws FileNotFoundException If <code>oggenc</code> could not be found in the <code>PATH</code>
  */ 
    public MPSinkOggenc(int cn, float f_frames, boolean verbose, String fn) throws FileNotFoundException {
	channels = cn;
	this.f_frames = f_frames;
	this.verbose = verbose;
	fileName = fn;

	String[] ps = System.getenv("PATH").split(File.pathSeparator);
	for ( int i=0; (oggencPath==null) && (i<ps.length); i++ ) {
	    File f = new File(ps[i],"oggenc");
	    if ( f.isFile() ) oggencPath = f.getAbsolutePath();
	}
	if ( oggencPath == null ) throw new  FileNotFoundException("Unable to find oggenc binary in the directories specified by PATH environment variable");
	if ( verbose ) MPLogger.log.println("Found oggenc binary: "+oggencPath);
    }

/** Return <code>-1</code> because there is no buffer size limit.
  * @return the maximum buffer size.
  */ 
    public int bufferSize() {
	return -1;
    }

/** checks whether process has been exited (check==true, throws IOException) and write write output of oggencProc.getInputStream() to stdout if verbose == true
  * returns true if process is still running
  */
    private boolean oggencRedirect(boolean check, boolean verbose) throws IOException {
	// check if process has been exited
        boolean run = false;
        boolean err = false;
        int ec = 0;
        try {
	    ec = oggencProc.exitValue();
	    err = check || (ec!=0);
    	} catch ( Exception e ) { 
    	    run = true;
    	}
	// read + redirect
	byte[] buf = new byte[1024];
	InputStream is = oggencProc.getInputStream();
	int i = 0;
	while ( is.available()>0 ) {
	    i = is.read(buf);
	    if ( (i>0) && (err || verbose) ) System.out.write(buf,0, i);
	}
	if ( (i>0) && (err || verbose) && (buf[i-1]!='\n') && ((i<2) || (buf[i-2]!='\n')) ) System.out.println();
	if ( err ) throw new IOException("oggenc exited unexpectedly with exit code " + ec);
	return run;
    }

/** Sends data to the oggenc process (blocking).
  * @param buf Frame buffer. Each frame contains one signed byte per channel.
  * @param len Length in frames.
  * @throws IOException If the process cannot be created or has been exited unexpectedly or with error.
  */ 
    public void sink(byte[] buf, int len) throws IOException {
	if ( oggencProc == null ) {
	    oggencProc = new ProcessBuilder( oggencPath, "-r", "-B", "8", "-C", String.valueOf(channels), "-R", String.valueOf(f_frames), "--advanced-encode-option", "disable_coupling", "--advanced-encode-option", "lowpass_frequency=9", "-q", "4", "-", "-o", fileName)
		    .redirectErrorStream(true)
		    .start();
    	    try {
    		Thread.sleep(200);
    	    } catch ( Exception e ) { }
	    if ( verbose ) MPLogger.log.println("Started oggenc process:");
	}
    	oggencRedirect(true,verbose);
	if ( verbose ) MPLogger.log.println("Writing " + Math.round(len/f_frames*1000) +"ms audio data to oggenc process");
	len*=channels;
	for ( int i=0; i<len; i++ ) 
	    buf[i]^=128;
	oggencProc.getOutputStream().write(buf, 0, len);
	for ( int i=0; i<len; i++ ) 
	    buf[i]^=128;
    }

/** Waites until <code>oggenc</code> has been finished.
  * @throws IOException If the process has been exited unexpectedly or with error.
  */ 
    public void close() throws IOException {
	if ( oggencProc != null ) {
	    oggencProc.getOutputStream().close();
	    do {
    		try {
    		    Thread.sleep(200);
    		} catch ( Exception e ) { }
	    } while ( oggencRedirect(false,verbose) );
	}
    }
}
