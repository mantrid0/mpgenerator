package mpgenerator;

import java.io.*;
import java.util.*;
import java.text.*;

import javax.sound.sampled.*;

// *****************************************************************************
// ******* MPGenerator *********************************************************
// *****************************************************************************
/** 
  * Common interface for the multi-phase EStim signal generators
  */
public interface MPGenerator {


/** 
  * Get the sample rate in Hz.
  * @return The sample rate in Hz.
  */
    public float fFrames();


/** 
  * Get the verbosity flag.
  * @return Whether other messages than errors and logs are printed.
  */
    public boolean verbose();


/** 
  * Return the number of sound channels 
  * @return The number of sound channels
  */  
    public int soundChannels();


/** 
  * Returns the sample period in audio frames
  * @return the sample period in audio frames
  */  
    public double samplePeriod();


/** 
  * Calculate the audio signals from a given source and write the result to a given sink.
  * @param src Phase data source.
  * @param sink Sound data sink.
  * @throws IOException If the audio data cannot be processed for any reason.
  */  
    public void run (MPSource src, MPSink sink) throws IOException;


/** 
  * Creates and returns an audio sink. If argument is null a {@link MPSinkPlay} instance is returned.
  * If argument ends with <code>.ogg</code> a {@link MPSinkOggenc} instance is returned. In all other cases a 
  * {@link MPSinkFile} instance is created.
  * @param fn File name, see above.
  * @return A new {@link MPSink} instance.
  * @throws IOException If the sink could not be created for any reason.
  */
    public default MPSink getMPSink (String fn) throws IOException {
	if ( fn == null ) return new MPSinkPlay(soundChannels(), fFrames(), verbose());
	else if ( fn.substring(fn.length()-4).equalsIgnoreCase(".ogg") ) return new MPSinkOggenc(soundChannels(), fFrames(), verbose(), fn);
	else return new MPSinkFile(soundChannels(), fFrames(), verbose(), fn);
    }

/** 
  * Creates and returns an audio sink for direct play back.
  * @return A new {@link MPSinkPlay} instance.
  * @throws IOException If the sink could not be created for any reason.
  */
    public default MPSinkPlay getMPSink () throws IOException {
	return new MPSinkPlay(soundChannels(), fFrames(), verbose());
    }


// ******* ParameterException **************************************************
// Exception that prints a help message
    static class ParameterException extends Exception {
	public final static String helpMsg = new String (
		"Usage: java -jar MPGenerator.jar [<options>] <script> [<positional parameter 1> [<positional parameter 2>] [...]]\n"+
		"Options:\n"+ 
		"    -c <name>    Configuration name. Configuration directory will be $(HOME)/MPGenerator/<name>.\n"+
		"                 If '\"\"' or '-' is specified, 'default' is used.\n"+
		"    -r <number>  HF Amplitude modulation frequency in Hz. This should be a slightly\n"+
		"                 above recognition limit. Usually something between 30 and 50 Hz,\n"+
		"                 default: 40.\n"+
		"    -d <number>  Duty ratio. Default 1. Smaller values only recommended with high-pass filtr.\n"+
		"    -g <number>[,<number>[,..]] Comma separated list of channels per group. Default: 2\n"+
//		"    -a <number>  Sound generation algorithm. Default 1\n"+
		"    -v <number>  Volume compensation. Valid: 0..1, Default: 0.5. \n"+
		"    -p <number>[,<number>[,..]] Comma separated list of phase offsets per group. Default: 0.5\n"+
		"    -f <number>  Carrier frequency in Hz. Default: 1000.\n"+
		"                 Valid: 200 to 2000. Recommended: 600 to 1200. \n" +
		"    -o <file>    Output file in .wav format. Default: play sound directly\n" +
		"    -q           Suppress all messages but warnings and errors.\n" +
		"    -h           Print this help message\n"
	    );
    
	public ParameterException (String msg) {
	    super( msg + "\n" + helpMsg );
	}

	public ParameterException () {
	    super( helpMsg );
	}
    }


/** 
  * Command line interface.
  * @param args Command line parameters
  */  
    public static void main(final String[] args) {
	try {
	    // scan command line
	    String ofn = null;			// output file name
	    String script = null;
	    double[] posPars = null;

    	    MPContext context = new MPContext();
	    
	    // parse all other parameters 
    	    for (int i=0; i<args.length; i++ ) {
    		String nextArg = i<args.length-1 ? args[i+1] : null;
    		String err = null;
    		if ( posPars != null ) {
    		    int j = posPars.length-args.length+i;
		    try {
			posPars[j] = (args[i].length()==0) ? 0 : MPContext.eval(args[i]);
    		    }
    		    catch (Exception e) {
		        throw new ParameterException("Error evaluating positional parameter " + (j+1) + " : "+ e.getLocalizedMessage());
		    }
		}
	        else if ( args[i].equals("-c") ) {
	    	    i++;
		    if (i>=args.length) throw new ParameterException("<name> expected after -c");
		    context.setConfigName( args[i] );
	    	}	
	        else if ( args[i].equals("-r") ) {
	    	    i++;
	    	    err = context.setConfig(context.confNameHighFrequency, nextArg);
		}
	        else if ( args[i].equals("-d") ) {
	    	    i++;
	    	    err = context.setConfig(context.confNameDutyRatio, nextArg);
		}
	        else if ( args[i].equals("-g") ) {
	    	    i++;
	    	    err = context.setConfig(context.confNameChannels, nextArg);
		}
/*	        else if ( args[i].equals("-a") ) {
	    	    i++;
	    	    err = context.setConfig(context.confNameAlgo, nextArg);
		} */
	        else if ( args[i].equals("-f") ) {
	    	    i++;
	    	    err = context.setConfig(context.confNameCarrierFrequency, nextArg);
		}
	        else if ( args[i].equals("-v") ) {
	    	    i++;
	    	    err = context.setConfig(context.confNameVolumeCompensation, nextArg);
		}
	        else if ( args[i].equals("-p") ) {
	    	    i++;
	    	    err = context.setConfig(context.confNameOffsets, nextArg);
		}
	        else if ( args[i].equals("-o") ) {
	    	    i++;
		    if (i>=args.length) throw new ParameterException("<filename> expected after -o");
	    	    ofn = args[i];
		}
	        else if ( args[i].equals("-q") ) {
	    	    context.verbose = false;
		}
		else if ( args[i].equals("-h") ) {
		    System.out.println(ParameterException.helpMsg);
	    	    System.exit(0);
		}
		else {
		    script = args[i]; 
		    if ( i<args.length-1 ) {
		        posPars = new double[args.length-1-i];
		        for ( int j=0; j<posPars.length; j++ )
		    	    posPars[j] = 0;
		    }
		}
		if ( err != null ) throw new ParameterException( err + " after " + args[i-1] );
	    }
	    if ( script == null) throw new ParameterException("<script 1> not defined");
	    context.writeConfigFile();

    	    MPGenerator gen = context.getGenerator();
    	    MPSink sink = gen.getMPSink(ofn);
	    context.readScript(gen,sink,script,posPars);
    	    sink.close();
	    System.exit(0);
	}
	catch ( ParameterException e ) {
	    System.err.println("Error: " + e.getLocalizedMessage() );
	} 
	catch ( IllegalArgumentException e ) {
	    System.err.println("Error: " + e.getLocalizedMessage() );
	} 
	catch ( Exception e ) {
	    System.err.println("Error: " + e.getLocalizedMessage() );
	    e.printStackTrace();
	}
	System.exit(1);
    }
    
}
