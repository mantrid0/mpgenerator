package mpgenerator;

import java.io.*;
import java.util.*;
import java.text.*;


// *****************************************************************************
// ******* MPSettings **********************************************************
// *****************************************************************************
/**
  * This class defines the parameters for the signal generation (see {@link MPSettings.S}) and stores them.
  */
public class MPSettings {

/** This enum describes the the parameters which are used for signals generated using the {@link MPSourceParam1} class.
  * A general description of the generated signals can be found at <a href="https://gitlab.com/mantrid0/mpgenerator/wikis/Tutorials">Wiki</a>.
  * <p>
  * The parameters can be divided in groups
  * <ul>
  * <li><b>a_*</b>: A-waveform, amplitude modulation
  * <li><b>b_*</b>: B-waveform, amplitude modulation
  * <li><b>r_*</b>: Periodic amplitude modulation
  * <li><b>p_*</b>: Phase modulation
  * </ul>
  * <p>
  * Many parameters can be best described in the image below. More details can be found 
  * in the descriptions starting at {@link #a_a0}.
  * <p>
  * <img src="{@docRoot}/settings.png" alt="Description of the parameters">
  */
    public enum S {

/** A-waveform amplitude modulation: volume during t1, see image at top.
  * The value is cropped to the usable range from 0 to 2.5.<br>
  * Default: 1 
  */
	a_a0,

/** A-waveform amplitude modulation: volume during t1, see image at top. 
  * The value is cropped to the usable range from 0 to 2.5.<br>
  * Default: 1 
  */
	a_a1,

/** A-waveform amplitude modulation: volume during t2, see image at top. 
  * Default: 1 
  */
	a_a2,

/** A-waveform amplitude modulation: a0 pulse width in s, see image at top. <br>
  * Valid: &ge; 0<br>
  * Default: 0 <br>
  */
	a_t0,

/** A-waveform amplitude modulation: a1 pulse width ins s, see image at top. <br>
  * Valid: &ge; 0<br>
  * Default: 0 <br>
  */
	a_t1,

/** A-waveform amplitude modulation: pause between sequences in s, see image at top. <br>
  * Valid: &ge; 0<br>
  * Default: 3 <br>
  * Randomized within -30% / +40%
  */
	a_t2,

/** A-waveform amplitude modulation: ar pulse width if ar is between 0 and 1, otherwise rise time. See image at top. <br>
  * Default: 1 
  */
	a_tr,

/** A-waveform amplitude modulation: number of pulses per sequence, see image at top. <br>
  * Valid: &ge; 1<br>
  * Default: 4 <br>
  * Randomized within +0%/-75%
  */
	a_n,


/** B-waveform amplitude modulation: amplitude, see image at top.
  * The modulation function varies between 1-r_a and 1. <br>
  * Valid: 0..1
  * Default: 0
  */
	b_a,

/** B-waveform amplitude modulation: width of low period in ms, see image at top.
  * Default: 25
  */
	b_t0,

/** B-waveform amplitude modulation: width of high period in ms, see image at top.
  * Default: 50
  */
	b_t1,

/** B-waveform amplitude modulation: phase shift. <br>
  * 0: No phase shift<br>
  * 1: Maximum phase shift<br>
  * Default: 0 <br>
  */
	b_s,

/** High frequency periodic amplitude modulation. <br>
  * Valid: 0..2
  * Default: 0
  */
	r,

/** Phase modulation amplitude. <br>
  * Valid: 0..1
  * Default: 0
  */
	p,

/** Phase modulation variation period in s. <br>
  * Valid: &gt; 1
  * Default: 5
  */
	p_T,

/** Low frequency effects. <br>
  * Valid: 0..1
  * Default: 0
  */
	l,
    }    

    // quick access index constants    
    protected static final int a_a0  = S.a_a0 .ordinal();	
    protected static final int a_a1  = S.a_a1 .ordinal();
    protected static final int a_a2  = S.a_a2 .ordinal();
    protected static final int a_t0  = S.a_t0 .ordinal();
    protected static final int a_t1  = S.a_t1 .ordinal();
    protected static final int a_t2  = S.a_t2 .ordinal();
    protected static final int a_tr  = S.a_tr .ordinal();
    protected static final int a_n   = S.a_n  .ordinal();

    protected static final int b_a   = S.b_a  .ordinal();
    protected static final int b_t0  = S.b_t0 .ordinal();
    protected static final int b_t1  = S.b_t1 .ordinal();
    protected static final int b_s   = S.b_s  .ordinal();

    protected static final int r     = S.r    .ordinal();
    protected static final int p     = S.p    .ordinal();
    protected static final int p_T   = S.p_T  .ordinal();
    protected static final int l     = S.l    .ordinal();

    // parameter storage
    protected double[] v = new double[S.values().length];

/** Creates a new instance in initialize it with default values */
    public MPSettings () {
	reset();
    }


/** Sets all parameters to its default values.
  */
    public void reset () {
	v[a_a0 ] = 1;		
    	v[a_a1 ] = 1;
    	v[a_a2 ] = 1;
    	v[a_t0 ] = 0;
    	v[a_t1 ] = 0;
    	v[a_t2 ] = 3;
    	v[a_tr ] = 1;
    	v[a_n  ] = 4;

    	v[b_a  ] = 0;
    	v[b_t0 ] = 25;
    	v[b_t1 ] = 50;
    	v[b_s  ] = 0;

    	v[r    ] = 0;
    	v[p    ] = 0;
    	v[p_T  ] = 5;
    	v[l    ] = 0;
   }


// Apply constraints
    private void constraints () {
    	v[a_tr] = Math.max(1e-8, v[a_tr]);
    	v[a_t0] = Math.max(0, v[a_t0]);
    	v[a_t1] = Math.max(0, v[a_t1]);
    	v[a_t2] = Math.max(0, v[a_t2]);
    }

/** Copy settings from another instance
  * @param s1 Source instance
  */
    public void set ( MPSettings s1 ) {
	for ( int i=0; i<v.length; i++ )
	    v[i] = s1.v[i];
	constraints();
    }

/** Linearly interpolate settings between two instances
  * @param s1 Source instance 1
  * @param s2 Source instance 2
  * @param r Interpolation parameter. At r=0 interpolation result is a copy of s1, at r=1 a copy of s2. 
  */
    public void set ( MPSettings s1, MPSettings s2, double r ) {
	r = Math.max(0, Math.min(1.0, r));
	for ( int i=0; i<v.length; i++ )
	    v[i] = (1-r)*s1.v[i] + r*s2.v[i];
	constraints();
    }
    
/** Set a parameter specified by its name.
  * The name is not case sensitive and '_' can be replaced by '.', i.e. these names refer to the same parameter: "r_t0", "r.t0", "R.T0".
  * @param s Parameter name
  * @param val Parameter value
  * @throws IllegalArgumentException if the parameter name is invalid
  */
    public void set ( String s, double val ) throws IllegalArgumentException {
	s = s.replace('.','_');
	S[] vs = S.values();
	for ( int i=0; i<vs.length; i++ )
	    if ( s.equalsIgnoreCase(vs[i].name()) ) {
		v[i] = val;
		return;
	    }
	throw new IllegalArgumentException("Invalid parameter: `"+s+"'");
    }

/** Get a parameter specified by its name.
  * The name is not case sensitive and '_' can be replaced by '.', i.e. these names refer to the same parameter: "r_t0", "r.t0", "R.T0".
  * @param s Parameter name
  * @return the value of the parameter.
  * @throws IllegalArgumentException if the parameter name is invalid
  */
    public double get ( String s) throws IllegalArgumentException {
	s = s.replace('.','_');
	S[] vs = S.values();
	for ( int i=0; i<vs.length; i++ )
	    if ( s.equalsIgnoreCase(vs[i].name()) ) return v[i];
	throw new IllegalArgumentException("Invalid parameter: `"+s+"'");
    }

/** Set a single parameter
  * @param s Parameter to set
  * @param val Parameter value
  */
    public void set ( S s, double val ) {
	v[s.ordinal()] = val;
    }

/** Get the value of a parameter
  * @param s Parameter to return
  * @return The value of the given parameter
  */
    public double get ( S s ) {
	return v[s.ordinal()];
    }
}
