package mpgenerator;

import java.io.*;
import java.util.*;
import java.text.*;

// *****************************************************************************
// ******* MPSink **************************************************************
// *****************************************************************************
/** Interface that defines a audio data sink,  */
public interface MPSink {
/** Get the maximum buffer size in frames that can be processed at once or <code>-1</code> if there is no limit.
  * @return the maximum buffer size in frames.
  */ 
    public int bufferSize();

/** Process the data. This method is allowed to be blocking (e.g. if the play buffer is full).
  * The input buffer <code>buf</code> must be reusable. If the data is not immediately processed the 
  * input data must be copied to an internal buffer.
  * @param buf Frame buffer. Each frame contains one signed byte per channel.
  * @param len Length in frames.
  * @throws IOException if the data cannot be processed for any reason.
  */ 
    public void sink(byte[] buf, int len) throws IOException;

/** Finishes all operation and releases resources. This method is allowed to be blocking (e.g. wait until all data is played).
  * @throws IOException if the data cannot be processed for any reason.
  */ 
    public void close() throws IOException;


/** 
  * Return whether the sink is active and accepting data.
  * @return true if the sink is active.
  */
    public default boolean isRunning () {
	return true;
    }

}

