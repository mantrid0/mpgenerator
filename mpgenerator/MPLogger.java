package mpgenerator;

import java.util.*;
import java.text.*;
import java.io.*;


// *****************************************************************************
// ******* MPLogger ************************************************************
// *****************************************************************************
/** Simple logger functionality */
public class MPLogger {
/** Defines how to write errors and warnings. */
    public static MPLogger err = new MPLogger(System.err);

/** Defines how to write normal log messages. */
    public static MPLogger log = new MPLogger(System.out);
    
/** 
  * Print formatted error message containing stack trace 
  * If <code>msg</code> is null only the stack trace will be printed.
  * Otherwise a message of the form <code>msg + ": " e.getLocalizedMessage()</code> will be printed.
  * @param msg The error message (can be null)
  * @param e Throwable containing the stack trace.
  */
    public final static void stackTrace(String msg, Throwable e) {
	StringWriter sw = new StringWriter();
	PrintWriter pw = new PrintWriter(sw);
	if ( msg != null ) pw.println();
	e.printStackTrace(pw);
	err.println(sw.toString().trim());	// trim removes trailing new line
    }


    
    private PrintStream ps;

/** 
  * Construct a ne instance.
  * @param ps Where to write the output to.
  */
    public MPLogger ( PrintStream ps ) {
	this.ps = ps;
    }
    
/** 
  * Prints the message to stderr.
  * @param s The message to print
  */
    public synchronized void println(String s) {
	ps.println(s);
    }
}
