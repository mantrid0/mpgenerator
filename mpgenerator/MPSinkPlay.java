package mpgenerator;

import java.io.*;
import java.util.*;
import java.text.*;

import javax.sound.sampled.*;

// *****************************************************************************
// ******* MPSinkPlay **********************************************************
// *****************************************************************************
/** Class that plays audio data directly to sound card */
public class MPSinkPlay implements MPSink {
    private final double bufTime = 2;		// maximum remaining play time before playing new data and buffer size
    private long playEnd = 0;			// posix time at which playback ends
    private long playStart = 0;			// posix time at which playback started
    private long oldPlayTime = 0;		// play time before playStart
    private int n_frames;			// desired buffer size in frames
    private int channels;
    private float f_frames;
    private boolean verbose;
    private SourceDataLine sourceDataLine = null;
    private boolean run = true;
    
/** 
  * Constructor. In most cases {@link MPGenerator#getMPSink(String)} should be used for creating an instance.
  * @param cn Number of channels
  * @param f_frames Frame rate
  * @param verbose be verbose.
  */ 
    public MPSinkPlay(int cn, float f_frames, boolean verbose) {
	channels = cn;
	this.f_frames = f_frames;
	this.verbose = verbose;
	n_frames = (int) Math.round(f_frames*bufTime);	
    }


/** 
  * Get the current play time in seconds.
  * @return Current play time.
  */ 
    public double playTime() {
	return Math.round( (oldPlayTime + Math.min(System.currentTimeMillis(),playEnd) - playStart) / 1000.0 );
    }


// wait until T. Return false if not waited.
    private boolean progress(long T) {
	long et = playEnd - T;
	long st = T - System.currentTimeMillis();
	boolean b = false;
	int  n = 0;
	while ( (st>0) && (sourceDataLine!=null) && run ) {
	    b = true;
	    if ( verbose && (n % 25==0) ) System.out.print("                   \rPlaying sound: " + Math.round(playTime()) + "s  (buffered: " + Math.round((st+et)/1000.0)+"s)" );
    	    try {
    		Thread.sleep(Math.min(20,st));
    	    } catch ( Exception e ) { }
    	    st = T - System.currentTimeMillis();
    	}
    	return b;
    }


    public int bufferSize() {
	return n_frames;
    }


/** 
  * Play back data. This method is blocking.
  * @param buf Frame buffer. Each frame contains one signed byte per channel.
  * @param len Length in frames
  * @throws IOException If the data cannot be processed for any reason.
  */ 
    public synchronized void sink(byte[] buf, int len) throws IOException {
	progress(playEnd-Math.round(bufTime*1000-500));
	if ( !run ) return;
	try {
	    if ( sourceDataLine == null ) {
		AudioFormat af = new AudioFormat(
    		    f_frames,
    		    8,  	// how many bits per sample
        	    channels,	// number of channels
    		    true,	// signed
    		    false	// little endian
    		);
		sourceDataLine = AudioSystem.getSourceDataLine(af);
    		sourceDataLine.open(af, 2*n_frames*channels );
	    }
	    sourceDataLine.write(buf, 0, len*channels);
    	    sourceDataLine.start();
	} catch ( Exception e ) {
	    throw new IOException("Unable to play sound: "+e.getLocalizedMessage());
	}
	long t = System.currentTimeMillis();
	if ( playEnd < t ) {
	    oldPlayTime += playEnd - playStart;
	    playStart = t;
	    playEnd = t + Math.round(1000.0*len/f_frames);
	} else {
	    playEnd += Math.round(1000.0*len/f_frames);
	}
    }


/** 
  * Waits until all data has been played and releases resources.
  */ 
    public void close() throws IOException {
	if ( sourceDataLine != null ) {
	    if (verbose) {
		progress(playEnd-500);
		System.out.println();
	    }
	    sourceDataLine.close();
	 }
    }

/** 
  * Aborts pending audio play back
  * @throws IOException If play back could not be aborted properly for any reason.
  */ 
    public void abort() throws IOException {
	if ( sourceDataLine != null ) {
	    sourceDataLine.close();
	    sourceDataLine = null;
	 }
	run = false;
    }

    public boolean isRunning () {
	return run;
    }
}
