package mpgenerator;

import java.util.*;
import java.text.*;
import java.io.*;


// *****************************************************************************
// ******* MPAudioServer *******************************************************
// *****************************************************************************
/** 
  * <code>MPAudioServer</code> creates implements a thread which is used for audio playback and control.
  * Using this class is the best way to integrate MPGenerator into other software.
  * <p>
  * Audio playback is started and stopped with {@link #setAudio(String,double[])}.
  */
public class MPAudioServer extends Thread {
    private int verbose = 1;
    
    // control variables
    private volatile boolean terminate;
    private volatile boolean isAlive;

    // MPGenerator object
    private static MPContext context = null;
    private static MPSinkPlay sink = null;
    private static MPGenerator gen = null;

    // source file. Changing this variable causes reload of audio
    private Reader srcIn = null;	// input
    private String srcName = null;	// name of the script
    private String srcDir = null;	// search directory for included scripts
    private double[] posParams = null;	// positional parameters

/**
  * Creates and starts the audio handler.
  * @param verbose Verbosity level. 0: only errors and warnings. 1: Errors, warnings and server messages. 2. All messages
  */
    public MPAudioServer(int verbose) {
	super();
	context = new MPContext();
	context.verbose = verbose > 1;
	this.verbose = verbose;
	start();
    }
    
/**
  * Stops the audio handler. This call waits up to 1.5s
  * @return true if the event handler terminated correctly.
  */
    public boolean terminate() {
	try {
	    setAudio( null, null );	// abort audio
	}
	catch ( Exception e ) { }
        terminate = true;
	for (int i=0; isAlive && i<=1500; i+=20 ) {
	    try { sleep(20); } catch ( InterruptedException e) { } 
	}
	return !isAlive;
    }

/**
  * Stop andio playback and load new audio file in MPGenerator script format. 
  * @param in Reader which is the script input or null in order to stop audio.
  * @param sname Name of the script used for generating error messages. If null, it will be replaced by "(anonymous)".
  * @param dir Search directory for included files. Can be null.
  * @param posParams Positional parameters for the audio file. Can be null.
  * @throws IOException if an error occurs while attempting to read the script.
  */
    private synchronized void setAudio ( Reader in, String sname, String dir, double[] posParams ) throws IOException  {
	if ( (sink!=null) || (gen!=null) ) {
	    if ( verbose > 1 ) System.out.println();
	    if ( verbose > 0 ) MPLogger.log.println("Stopping audio playback");
    	    try  {
		if ( sink != null ) sink.abort();
		gen = null;
		sink = null;
    	    } catch ( Exception e ) {
    		MPLogger.err.println ( "Error in audio handler: " + e.getLocalizedMessage() );
    	    }
    	}
	this.srcIn = in;
	this.srcDir = dir;
	this.srcName = ( sname==null) ? "(anonymous)" : sname;
	this.posParams = posParams;
    }

/**
  * Stop andio playback and load new audio script as a String
  * @param scr Audio Script.
  * @param dir Search directory for included files. Can be null.
  * @param posParams Positional parameters for the audio file. Can be null.
  * @throws IOException if an error occurs while attempting to read the script.
  */
    public synchronized void setAudio ( String scr, String dir, double[] posParams ) throws IOException  {
	setAudio(new StringReader(scr),null,dir, posParams);
    }

/**
  * Stop andio playback and load new audio file in MPGenerator script format. 
  * @param src Name of audio file or null in order to stop audio
  * @param posParams Positional parameters for the audio file. Can be null.
  * @throws IOException if an error occurs while attempting to read the script.
  */
    public synchronized void setAudio ( String src, double[] posParams ) throws IOException {
	String dir = null;
	Reader in = null;
	if ( src != null ) {
	    dir = new File(src).getParent();
	    in = new FileReader(src);
	}
	setAudio(in,src,dir,posParams);
    }

/** 
  * Get the {@link MPContext} class for this thread.
  * @return Return the {@link MPContext} class for this thread.
  */
    public MPContext context() {
	return context;
    }

/** 
  * Returns whether sound is played
  * @return True if sound is played
  */
    public boolean isPlaying() {
	return (sink!=null) && sink.isRunning();
    }

/**
  * The thread body.
  */
    public void run() {
	isAlive = true;
	terminate = false;
	Reader pin = null;
	if ( verbose > 0 ) MPLogger.log.println("Starting audio loop");
        while ( ! terminate ) {
    	    try  {
    		if ( pin != srcIn ) {
    		    pin = srcIn;
    		    if ( pin!=null ) {
    			gen = context.getGenerator();
    			sink = gen.getMPSink();
			if ( verbose > 0 ) MPLogger.log.println("Starting playback of '" + srcName + "'");
    			context.readScript(gen,sink, srcIn,srcName,srcDir, posParams);
    		    }
    		}
    		sleep(50);
    	    } catch ( InterruptedException e ) {
    	    } catch ( Exception e ) {
    		MPLogger.err.println ( "Error in audio handler: " + e.getLocalizedMessage() );
    	    }
        }
	isAlive = false;
	if ( verbose > 0 ) MPLogger.log.println("Audio loop stopped");
    }
}
