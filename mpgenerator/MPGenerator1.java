package mpgenerator;

import java.io.*;
import java.util.*;
import java.text.*;


// *****************************************************************************
// ******* MPGenerator1 ********************************************************
// *****************************************************************************
/** Generates multi-phase Estim signals with additive channel mixing. Instances of this class are created by {@link MPContext#getGenerator()}.
  */
public class MPGenerator1 implements MPGenerator {
    private final static int f_frames = 44100;		// sample rate in Hz

    private int sample_period;	  	  		// standard sample period in frames
    
    private int cn;					// number of channels
    private int pulse_width;				// max. pulse width in frames. equal to half period of carrier signal
    private boolean verbose = true;
    
    private long sample_count = 0;
    
    // sin buffer, length is 256, corresponds to pulse_width
    private short[] sbuf = new short[256];

    public float fFrames() {
	return f_frames;
    }

    public double samplePeriod() {
	return sample_period;
    }

    public boolean verbose() {
	return verbose;
    }

    public int soundChannels() {
	return cn;
    }

/** 
  * Constructor
  * @param cn Number of channels
  * @param f_max Maximum frequency
  * @param cf Desired carrier frequency in Hz.
  * @param dr Desired duty ratio
  * @param v Be verbose.
  * @throws IllegalArgumentException if channel configuration is invalid
  */
    protected MPGenerator1 (int cn, double f_max, double cf, double dr, boolean v) throws IllegalArgumentException {
	verbose = v;
	this.cn = cn;
	
	// calculate waveform
	cf = Math.max(8*f_max, Math.min(2000, Math.abs(cf)));	// 8*f_max ... 2000
	dr = Math.min(1.0,Math.max(0.1,Math.abs(dr))); 		// 0.1..1
	pulse_width = (int) Math.round(f_frames/(cf*2));
	sample_period = (int) Math.round(Math.min(pulse_width*4/dr, f_frames/(4*f_max)));
	if ( v ) MPLogger.log.println("Sampling frequency: "+Math.round(f_frames/sample_period) +" Hz   duty ratio: "+Math.round(400*pulse_width/sample_period)+"%");

	for (int i=0; i<256; i++ ) 
	    sbuf[i] = (short) Math.round(Math.sin(i*Math.PI/256.0)*1024);
    }


/** 
  * Calculate the audio signals from a given source and write the result to a given sink.
  * @param src Phase data source.
  * @param sink Sound data sink.
  * @throws IOException If the audio data cannot be processed for any reason.
  */  
    public void run (MPSource src, MPSink sink) throws IOException {
	if ( !sink.isRunning() ) return;
	int frames_n = (int) Math.round(src.available()*f_frames/sample_period+0.51) * sample_period;		// number of frames

	// frame buffer
	int fbuf_ptr = 0;
	int fbuf_size = sink.bufferSize();
	if ( fbuf_size<0 ) fbuf_size = frames_n;
	byte[] fbuf = new byte[fbuf_size*cn];
	
	short[] chc = new short[cn*4];				// modulation coefficients per audio channel
	double[] chp = new double[cn*3];			// amplitude (0..1) and sin(phase) per channel 
	
	double r1,r2;
	int t,c,sr,i,j,i1,i2,i3,rsr,pwm;
	double dt = sample_period * 1.0 / f_frames;

	// signal calculation
	sr = sample_period;
	rsr = 256*1024/pulse_width;
	for ( t = 0; (t<frames_n) && sink.isRunning(); t++ ) {
	    if ( sr >= sample_period ) {
		src.getSample(dt, sample_count, chp);
		sample_count++;
		sr = 0;
		// modulation coefficients per audio channel
		for ( c=0; c<cn; c++ ) {
		    // amplitude modulation
		    chc[c*4  ] = (short) Math.round( Math.sqrt( Math.max(0, Math.min(1, chp[c*3])) )*1023.4 );
		    // phase shift
		    r2 = chp[c*3+1]/(2*Math.PI);
		    r2 -= Math.round(r2);
		    chc[c*4+1] = (short) (512 + Math.round(r2*512));				// sinusoidal phase shift
		    chc[c*4+2] = (short) (512 + Math.round(r2*512));				// rectangular and triangular shift
//		    chc[c*4+2] = (short) (512 + Math.round(Math.sin(r2*2*Math.PI)*256));	// rectangular and triangular shift
		    // weight factors
		    r2 = Math.max(0.5, Math.min(2.0, chp[c*3+2]))-1;
		    chc[c*4+3]= ( r2<0 ) ? 
			(short) Math.round( r2*3*1024 ) :				// triangular weight
			(short) Math.round( r2*1024 );					// rectangular weight
		}
	    }
	    // modulation
	    j = (sr*rsr) >> 10;
	    sr++;
 	    i3 = fbuf_ptr*cn;
	    if (j<1024 ) {
		for ( c=0; c<cn; c++ ) {
		    pwm = chc[c*4+3];
		    // sinusoidal part
		    i2 = j + chc[c*4+1];
		    i1 = sbuf[i2 & 255]*(1-((i2 & 256) >> 7));
		    // triangular and rectangular part
	    	    i2 = j + chc[c*4+2];
		    if ( pwm < 0 ) {
			pwm = -pwm;
			i2 = (1024 - Math.abs(((i2 & 255)<<3)-1020)) * (1-((i2 & 256) >> 7));	// triangular part
		    } else {
			i2 = 1024 - ((i2 & 256) << 3);						// rectangular part
		    }
	    	    fbuf[i3+c] = (byte) ((( i1*(1024-pwm) +  i2*pwm ) * chc[c*4] ) >> 23);
	    	}
	    } 
	    else {
		for ( c=0; c<cn; c++ ) 
		    fbuf[i3+c] = 0;
	    }
	    // sink if necessary
	    fbuf_ptr++;
	    if ( fbuf_ptr>=fbuf_size ) {
		if ( sink.isRunning() ) sink.sink(fbuf, fbuf_size);
		fbuf_ptr = 0;
	    }
	}
	if ( (fbuf_ptr>0) && sink.isRunning() ) sink.sink(fbuf, fbuf_ptr);
    }
}
