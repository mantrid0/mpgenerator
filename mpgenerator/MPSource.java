package mpgenerator;

import java.io.*;
import java.util.*;
import java.text.*;

// *****************************************************************************
// ******* MPSource ************************************************************
// *****************************************************************************

/** Interface that defines a source of phase data */
public interface MPSource {
/** Returns the remaining time
  * @return the remaining time
  */ 
    public abstract double available();
    
/** Get modulation data for one data sample.
  * Amplitude modulation, phase modulation and pulse width modulation are supported.
  * @param dt The standard sample width in seconds. For frequency modulation this value can be modified within range 2/3*dt0 to 2*dt0. Resulting sample width is returned. 
  * @param cnt Sample count.
  * @param chp The buffer where to store amplitude and phase modulation data. 
  * <code>chp[3*n]</code> is the amplitude modulation for channel <code>n</code>. Valid values are 0 to 1.
  * <code>chp[3*n+1]</code> is the phase modulation for channel <code>n</code> in radians.
  * <code>chp[3*n+2]</code> Pulse width modulation for channel <code>n</code>. It's the integral of the power (amplitude in square) of the carrier waveform divided by &pi;. In practice 
  * a value of 1 results in a sinusoidal signal, a value of 2 results in a rectangular signal and a value of 2/3 results in a triangular signal.  Valid values are 0.5 to 2.
  */ 
    public abstract void getSample(double dt, long cnt, double[] chp);

}    
