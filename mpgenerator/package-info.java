/** 
MPGenerator is a software for generation of Multi-phase Estim sound signals, i.e. it supports amplitude, phase and frequency modulation.
<p>
It can either be used as an API or as a standalone program which can output the signals to a sound card or write them to a sound file file.
<p>
This documentation covers the API. The usage of the standalone program is described at <a href="https://gitlab.com/mantrid0/mpgenerator/blob/master/Readme.md">Readme.md</a>.
*/

package mpgenerator;
