package mpgenerator;

import java.io.*;
import java.util.*;
import java.text.*;


// *****************************************************************************
// ******* MPSourceParam1 ******************************************************
// *****************************************************************************
/** Defines a data source defined by the parameter set described in {@link MPSettings.S}. */
public class MPSourceParam1 extends MPSourceParam {
    private double vc = 0.5;				// volume compensation: 0..1

    private double r_f;					// HF amplitude modulation frequency
    private double[] gpos;                              // 0..cng-1: channel position within group for b-waveform calculation. Period is 1
    private int[] f_div;                                // 0..cnc-1: frequency divider for LF effects
    private int[] cpg;                                  // 0..cng-1: channels per group
    private double[] opc_cs;                            // 0..cnc-1: offset for each channel as unity circle coordinates: sqr(opc_cs[i*2])+sqr(opc_cs[i*2+1]) = 1
    
    // states
    private double r_p = 0;				// phase of HF amplitude modulation, period is 1
    private long r_c = 0;				// period counter of HF amplitude modulation
    private double[] p_p;                               // 0..cnc-1: phase shift, 0..1
    private double[] p_f;                               // 0..cnc-1: phase shift frequency
    private double[] p_f_m;                             // 0..cnc-1: maximum phase shift frequncy
    private double[] p_f_p;                             // 0..cnc-1: phase shift frequency phase. 0..1
    protected double b_p = 0;				// phase of rectangular amplitude modulation waveform, period is 1
    
/**
  * Constructor
  * @param fmax HF Amplitude modulation frequency. This should be a slightly above the recognition limit.
  * @param vc Volume compensation. Cropped to valid range of 0...1
  * @param cpg Channels per group
  * @param opg Offsets per group, default: 0.5
  * @param verbose Be verbose.
  * @throws IllegalArgumentException if channel configuration is invalid
  */
    public MPSourceParam1(double fmax, double vc, int[] cpg, double[] opg, boolean verbose ) throws IllegalArgumentException {
	super(cpg,verbose);
	r_f = fmax;
	this.vc = Math.max(0, Math.min(1, vc));
	gpos = new double[cng];
	this.cpg = new int[cng];
	int cnc=0;
	for ( int i=0; i<cng; i++ ) {
	    this.cpg[i] = Math.abs(cpg[i]);
	    gpos[i] = i/(double)cng;
	    cnc+=this.cpg[i];
    	}
	
	// offsets per channel
	opc_cs = new double[cnc*2];
	int k=0;
	for ( int i=0; i<cng; i++ ) {
	    int l = this.cpg[i];
	    double o = ( ( (opg!=null) && (i<opg.length)) ? Math.max(0, Math.min(1, opg[i])) : 0.5 ) *2*Math.PI / l;
	    System.out.print("Channel offsets for group "+(i+1)+":"+l);
	    for ( int j=0; j<l; j++ ) {
		opc_cs[k*2]   = Math.cos((j-0.5*(l-1))*o);
		opc_cs[k*2+1] = Math.sin((j-0.5*(l-1))*o);
		System.out.print(String.format(Locale.US, " %.2f ",Math.atan2(opc_cs[k*2+1],opc_cs[k*2])/Math.PI*180));
		k++;
	    }
	    System.out.println();
	}

    	// phase modulation states
	p_p   = new double[cnc];
	p_f   = new double[cnc];
	p_f_p = new double[cnc];
	p_f_m = new double[cnc];
	k=0;
	for ( int i=0; i<cng; i++ ) {
	    int l = this.cpg[i];
	    double r = 1.0 / l;
	    double s = ((l<3) ? 0.5 : 1/Math.sqrt(3))  * r_f*0.5;
	    for ( int j=0; j<this.cpg[i]; j++ ) {
		p_p[k] = 0.5*j*r;
		p_f_p[k] = j*r;
		p_f_m[k] = s;
		k++;
	    }
	}
    }

    public void getSample(MPSettings s, double dt, long cnt, double[] chp) {
	int i,j,div,sh;
	double a,p,q,r,r0,r1,r2,r3,r4,r5,r6,r7,r8,a0,a1,a2,x,y;
	double ar,dr,pmo,pmr,pmb,pm,bf,br;
	
	// lowest pulse width modulation
	pmo = (vc*3>1) ? 2.0/(1.0+3*vc) : 1; 

	// HF amplitude modulation modulation
	q = Math.max(0, Math.min(2, s.v[s.r]));	// HF amplitude modulation parameter
	pmr = ( q < 1 ) ? q/(2.0-q) : (q > 1.5) ? 4*q-5 : 1.0;		// pulse width modulation, HF part
	dr = 0.5 - 0.5*Math.max(0.0,q-1.5);				// duty ratio: 0.5 .. 0.25
	r = r_f;
	if ( q>1.5 ) r *= 0.5;
	else if ( q>1 ) r /= 2*q-1;
	r_p += r*dt;
	r_c += Math.round(r_p);
	r_p -= Math.round(r_p);
	ar = (r_p<0.5-dr) ? Math.max(0,1-q) : 1;			// HF amplitude modulation
	
	// b_p: phase of rectangular amplitude modulation waveform
	r1 = s.v[s.b_t0];
	r2 = Math.max(1,s.v[s.b_t1]);
	r5 = s.v[s.b_a];
	b_p += 1000.0*dt/(r1+r2);
	b_p -= Math.round(b_p);
	bf = 2000.0/((r1+r2)*r_f);			// frequency coefficient for pulse width calculation: bf = 2*f_b/f_max
	br = Math.max(0,r1/r2*r5);			// duty cycle coefficient for pulse width calculation

	// channel data
	r0 = r1/(r1+r2) - 0.5;
	a0 = s.v[s.a_a0];
	a1 = s.v[s.a_a1];
	a2 = s.v[s.a_a2];
	r3 = Math.min(2.0,Math.max(0, s.v[s.p]));
	r4 = s.v[s.b_s];
	r5 = Math.max(0,1.0-r5);
	r6 = Math.min(Math.min(1,r5), Math.max(0, s.v[s.l]));
	r7 = 0.5*r_f;
	r8 = 0.5/Math.max(1.0, s.v[s.p_T]);
	i = 0; 	j = 0; 
	div = 1;
	for ( int g=0; g<cng; g++ ) {
	    div*=2;
	    // a-waveform modulation
	    a = a_buf[g];
	    a = a>1 ? ( a1 + (a2-a1)*(a-1) ) : ( a0 + (a1-a0)*a );
	    // b-waveform modulation
	    q = b_p + r4*gpos[g];
	    a *= (q-Math.round(q)) < r0 ? r5 : 1.0;
	    pmb = br*Math.min(1,bf*(1<<i));		// pulse width modulation, b-part: pmb = t_0/t_1*b_a * min( 1, 2*f_b/(f_max/div) )
	    // pulse width modulation
	    pm = pmo * ( 1 + (pmr+pmb)*vc );
	    // channel calculation
	    for ( int c=0; c<cpg[g]; c++ ) {
		// low frequency effects
/*		if ( c == 0 ) {
		    r = ar;
		} else {
		    div = 1 << c;
		    r = r_c % div + 0.5 + r_p; 
		    r = ( r > (1-dr)*div ) ? 1 : 0;
		    r = r6*r + (1-r6)*ar;
		} */
		if ( c>1 ) div*=2;
		r = (c==0 ? r_c+(div >> 1): r_c) % div + 0.5 + r_p; 
		r = ( r > (1-dr)*div ) ? 1 : 0;
		r = r6*r + (1-r6)*ar;

		chp[i*3+0] = a * r * a_prescale;
		// phase modulation
		r = p_f_p[i] + r8*dt;
		r -= Math.round(r);
		p_f_p[i] = r;
		r = p_f_m[i]*Math.sin(r*2*Math.PI);
//		r = i==0 ? -0.5 : 0.5; 
		q = p_p[i] + r*dt;
		q -= Math.round(q);
		p_p[i] = q;
		q *= 2*Math.PI;
		r = Math.min(r3,1);
		x = Math.cos(q)*r;
		y = Math.sin(q)*r;
		// solve sqr(x+r*o_x) + sqr(y+r*o_y) = 1
		r = x*opc_cs[i*2  ] + y*opc_cs[i*2+1];
		r = Math.sqrt(1.0 - x*x - y*y + r*r)-Math.abs(r);
		x += r*opc_cs[i*2  ];
		y += r*opc_cs[i*2+1];
		if ( r3>1 ) {
		    r = Math.sqrt(2-r3);
		    x = r*x;
		    y = r*y + Math.sqrt(r3-1)*Math.signum(y);
		} 
		chp[i*3+1] = Math.atan2(y,x);
		// pulse width
		chp[i*3+2] = pm;
		i++;
	    }
	}
    }
}
