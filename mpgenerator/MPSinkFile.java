package mpgenerator;

import java.io.*;
import java.util.*;
import java.text.*;

import javax.sound.sampled.*;

// *****************************************************************************
// ******* MPSinkFile **********************************************************
// *****************************************************************************
/** Class that writes audio data to a .wav file */
public class MPSinkFile implements MPSink {
    private int channels;
    private float f_frames;
    private boolean verbose;
    private String fileName;
    private Vector<byte[]> bufVect;
    
/** Constructor. In most cases {@link MPGenerator#getMPSink(String)} should be used for creating an instance.
  * @param cn Number of channels
  * @param f_frames Frame rate
  * @param verbose be verbose.
  * @param fn File name. Can be null. In this case data must be written using {@link #write} method.
  */ 
    public MPSinkFile(int cn, float f_frames, boolean verbose, String fn) {
	channels = cn;
	this.f_frames = f_frames;
	this.verbose = verbose;
	fileName = fn;
	bufVect = new Vector<byte[]>();
    }

/** Writes data to file
  * @param fn File name.
  * @throws IOException if an I/O error occurs.
  */ 
    public void write(String fn) throws IOException {
	Vector<ByteArrayInputStream> isv = new Vector<ByteArrayInputStream>();
	long l = 0;
	for ( byte[] b: bufVect ) {
	    isv.add( new ByteArrayInputStream(b) );
	    l+=b.length;
	}
	AudioFormat af = new AudioFormat(
    	    f_frames,
    	    8,  	// how many bits per sample
            channels,	// number of channels
    	    true,	// signed
            false	// little endian
    	);
        AudioInputStream as = new AudioInputStream(new SequenceInputStream(isv.elements()), af, l/channels);
    	if ( verbose ) MPLogger.log.println("Writing "+fn);
    	try {
    	    AudioSystem.write(as, AudioFileFormat.Type.WAVE, new File(fn));
    	} catch ( IllegalArgumentException e ) {  // should not happen
	    e.printStackTrace();
    	    throw new IOException( e.getLocalizedMessage() );
    	}
    }

/** Return <code>-1</code> because there is no buffer size limit.
  * @return the maximum buffer size.
  */ 
    public int bufferSize() {
	return -1;
    }

/** Copies the data to internal buffer (non-blocking).
  * @param buf Frame buffer. Each frame contains one signed byte per channel.
  * @param len Length in frames.
  */ 
    public void sink(byte[] buf, int len) {
	byte[] buf2 = new byte[channels*len];
	System.arraycopy(buf,0, buf2,0, buf2.length);
	bufVect.add(buf2);
	if ( verbose ) MPLogger.log.println("Generated " + Math.round(len/f_frames) +"s audio data");
    }

/** Writes data to file (if {link #MpSinkFile} was called with <code>fn!=null</code>) and releases all resources.
  * @throws IOException if an I/O error occurs.
  */ 
    public void close() throws IOException {
	if ( fileName != null ) write(fileName);
	bufVect.clear();
    }
}
