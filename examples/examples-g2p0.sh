#!/bin/bash

set -e 

for f in *.mps; do
    wav=${f%*.mps}-g2p0.wav
    java -jar ../MPGenerator.jar -g 2 -p 0 -o $wav $f
done    
