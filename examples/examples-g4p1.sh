#!/bin/bash

set -e 

for f in *.mps; do
    wav=${f%*.mps}-g4p1.wav
    java -jar ../MPGenerator.jar -g 4 -p 1 -o $wav $f
done    
