# shock patterns with amplitude slowly increasing from 1 to 1.5
# rectangular shape, avg. duty  ratio 50%

a.a0=1
a.a1=0
a.t0=0.125
a.tr=0
a.t1=0.125
a.n=20
a.t2=3

run 3

#######

a.a0=1.5

run 31

#######

run 6


