#!/bin/bash

set -e 

if [ "$1" = "" ]; then
    echo "Usage: $0 <suffix, e.g. 11, 11b>"
    exit 1
fi

./test-$1.sh

for f in *-$1.wav; do
    wav=${f%*.wav}m.wav
    if [ -f "$f" ]; then
	java -cp ../MPGenerator.jar mpgenerator/MPSimulator -m $f $wav
    fi
done    
