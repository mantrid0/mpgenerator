#!/bin/bash

set -e 

for f in *.mps; do
    wav=${f%*.mps}-g4p0.wav
    java -jar ../MPGenerator.jar -g 4 -p 0 -o $wav $f
done    
