#!/bin/bash

set -e 

for f in *.mps; do
    wav=${f%*.mps}-g3.wav
    java -jar ../MPGenerator.jar -g 3 -p 0.5 -o $wav $f
done    
