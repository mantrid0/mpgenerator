#!/bin/bash

set -e 

for f in *.mps; do
    wav=${f%*.mps}-g1.wav
    java -jar ../MPGenerator.jar -g 1 -p 0.5 -o $wav $f
done    
