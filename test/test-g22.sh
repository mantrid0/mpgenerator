#!/bin/bash

set -e 

for f in *.mps; do
    wav=${f%*.mps}-g22.wav
    java -jar ../MPGenerator.jar -g 2,2 -p 0.5 -o $wav $f
done    
