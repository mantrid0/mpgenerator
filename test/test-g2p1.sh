#!/bin/bash

set -e 

for f in *.mps; do
    wav=${f%*.mps}-g2p1.wav
    java -jar ../MPGenerator.jar -g 2 -p 1 -o $wav $f
done    
