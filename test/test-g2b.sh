#!/bin/bash

set -e 

for f in *.mps; do
    wav=${f%*.mps}-g2b.wav
    java -jar ../MPGenerator.jar -g 2 -p 0.5 -d 0 -o $wav $f
done    
