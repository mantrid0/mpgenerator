This project has been suspended because EStim audio generation was re-implemented in JavaScript
and is now a part of the GAsm RTL (guide assembler runtime library). Thus, the MPGenerator
software and the GServer became obsolete. GAsm has been forked into a new project:
<https://gitlab.com/mantrid0/gasm>


MPGenerator
===========

MPGenerator is a software for generation of Multi-phase Estim sound signals, i.e. it supports 
amplitude, phase and pulse width modulation.

It can either be used as an API or as a standalone program which can output the signals to a sound
card or write them to a sound file.

The audio data is defined by an efficient parameter set.

This documentation describes the project in general. The API documentation can be found at 
[API documentation](https://mantrid0.gitlab.io/mpgenerator/api/index.html).

There is also a sound library which is described at
[Milovana: EStim sound library](https://milovana.com/forum/viewtopic.php?f=2&t=21864)

Documentation of gasm, an assembler for interactive web guides, can be found at 
[gasm/Readme.md](https://gitlab.com/mantrid0/mpgenerator/blob/master/gasm/Readme.md) 


Contents
--------
* [Download](#download)
* [The Theory](#the-theory)
* [MPGenerator (command line tool)](#mpgenerator-command-line-tool)


Download
--------
To run the standalone programs only the Jar Archive is needed: 
[MPGenerator.jar](https://gitlab.com/mantrid0/mpgenerator/blob/master/MPGenerator.jar) 

If you also are interested in the example it easiest to download the whole repository: 
[mpgenerator-master.zip](https://gitlab.com/mantrid0/mpgenerator/-/archive/master/mpgenerator-master.zip)


The Theory
----------

### Voltage vs. current
Neurons react on voltage, but differential voltage, not absolute voltage. Because differential 
voltage is proportional to the current, a good Estim boxes provides a current controlled output
signal. That's also the reason why small contact surfaces / bad contact resistance feel stinging
(high current density).

### Carrier waveform and safety
A Estim signal must only contain high frequency components because low frequencies can cause 
electrolysis which can be very dangerous. Therefore Estim signals consist in a modulated carrier 
waveform which only contains frequency components above a few hundred Hz. (The upper frequency limit
is defined by the minimum pulse width required by the neuron to react. This is about 200µs). 

A save Estim box therefore either contains a high-pass filter or rectifies, filters and re-modulates
the input audio signal. The latter kind of devices is not recommended for the sound files generated
by MPGenerator (or many other Estim sound files) because they can't preserve phase modulation (see
below) or other special effects.

The frequency components of the modulation waveform does not appear in the spectrum of the modulated
 output signal.

### Amplitude modulation
The stimulation intensity at the contact surface of the electrodes directly depends form the
amplitude of the modulation waveform, in particular its approximately proportional in square
(doubling the volume increases intensity by about factor 4).

If more than one channels is connected to one electrode (see below) the intensity depends on the sum
 of the signals.

### Phase modulation
Each output channel has two poles. If one pole of each channel is connected to a pole of another
channel and if this pole is used as an electrode something magically happens: the path of the 
internal currents vary. This effect is often described as the "3D effect". 

In particular, the internal currents are a mix of the channel signals. If the carrier wave forms are
shifted against each other they are either amplified (phase shift between -90° and 90°) or damped 
(phase shift between 90° and 270°). This phase shift is called phase modulation.

Conclusion: Intensity at the contact surface depends on amplitude modulation, stimulation of the
internal neurons depends on amplitude and phase modulation.

Unfortunately there aren't much Estim sound files the make use of this technique. That's the reason
why this project was started.

### Pulse width modulation
This defines the wave form of the carrier signal. Its something between a sinusoidal signal (pulse
width 1) and a rectangular signal (pulse width 2).

### Modulation frequencies
Neurons can only detect variations up to a certain speed. Modulation frequency above that limit feel
like a constant signal. This limit frequency depends on the area of the body. It's highest (up to
about 30Hz to 50Hz) at the most sensitive areas.

It turned out that modulation signals close or slightly above that limit feel very intense (in a 
good way), very low frequencies (below a few Hz) feel good too but medium frequencies can be 
uncomfortable. (That's an effect of neuronal inhibition).

MPGenerator takes these effects into account.

MPGenerator (command line tool)
-------------------------------

This program creates audio data from script files which can be played back directly or saved as file. 

The usage is 

```java -jar MPGenerator.jar [<options>] <script> [<positional parameter 1> [<positional parameter 2>] [...]]```

The script file is given by `<script>` and can have positional parameters. 

### Options

#### `-c <name>`
Specifies a configuration name which is used to determine a directory for storing configuration
data. Directory name will be `$(HOME)/MPGenerator/<name>`, where `$(HOME)` is the user directory.

If this option is not specified, configurations are neither loaded or saved.

#### `-o <file>`
Output file in .wav format or .ogg format (requires `oggenc`). If not given the sound is played.

#### `-f <number>`
Carrier frequency in Hz. Default is 800. Recommended range is 600 to 1200. Valid range is 200 to 
2000.

#### `-g <number>[,<number>[,..]] `
Comma separated list of channels per group. 
Sound generation algorithm 2 only supports 2 channels per group, i.e. only the number of groups is
variable for this algorithm.

#### `-a <number>`
Sound generation algorithm. Default 1.

#### `-v <number>`
Volume compensation. Valid: 0..1, Default: 0.5. 

#### `-p <number>`
Phase modulation frequency multiplier. Default: 1. 

#### `-q`
Disables all messages but warnings and errors.

#### `-h`
Print a help message.

    
### Examples

Convert `examples/ex02.mps` to `ex02.wav` and produce a 2 channel output. Channel frequencies are
40 Hz and 10 Hz.

```java -jar MPGenerator.jar 40 10 -o ex02.wav - examples/ex02.mps```

Play back `examples/ex02.mps` with 4 channel signal consisting in 2 groups each consisting in 2
channels. Group frequencies are 40 Hz and 10 Hz, maximum phase shifts within group are 60° and 120°.

```java -jar MPGenerator.jar 40,60 10,120 - examples/ex02.mps```

Convert `examples/ex02.mps` to `ex02.ogg` and produce a 4 channel output consisting in one 3-channel
group and one single channel (1-channel group). Channel frequencies are 40 Hz for the group an and 
10 Hz the single channel. Maximum phase difference within the group is 90°.

```java -jar MPGenerator.jar 40,90,3 10 -o ex02.ogg - examples/ex02.mps```
