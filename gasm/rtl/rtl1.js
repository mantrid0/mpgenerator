/** @file  rtl1.js
  * This is the high level part of the gasm run time library.
  */

'use strict';

/* todo:
    * timer style
*/    

/** 
  * Directory where to search for images.
  */
scriptVars.imageDir = ".";

/** 
  * Directory where to search for videos.
  */
scriptVars.videoDir = ".";

/** 
  * Directory where to search for audio files.
  */
scriptVars.audioDir = ".";

/** 
  * Current image or empty string if no image is displayed.
  */
scriptVars.imageSrc = "";

/**
  * Current video file or empty string if no video is played.
  */
scriptVars.videoSrc = "";

/**
  * Current audio file or empty string if no sound is played.
  */
scriptVars.audioSrc = "";

/**
  * Set it to true in order to update buttons 
  */
var updateButtonsRequired = false;

/**
  * Init user script variables. This function is empty by default and can be overwritten by user.
  */
function initScriptVars () {
}

/*
 * Init estim script variables. This will be overwritten by EStim API.
 */
function initScriptVarsEstim () {
}

// Init high level RTL script variables. Called by initScriptVars0.
function initScriptVars1 () {
    scriptVars.imageDir = ".";
    scriptVars.videoDir = ".";
    scriptVars.audioDir = ".";
    scriptVars.imageSrc = "";
    scriptVars.videoSrc = "";
    scriptVars.audioSrc = "";
    if ( (typeof( baseDirectory ) != 'string') ) baseDirectory =".";
    scriptVars.buttons = {
	actions : [],
	names : [],
    };
    scriptVars.backupDOM = {
	text : "",
	media : "",
	audio : ""
    };
    initScriptVarsEstim();
    // user defines script variables
    initScriptVars();
    updateButtonsRequired = true;
}


// *****************************************************************************
// ******* DOM *****************************************************************
// *****************************************************************************
/** 
  * Convenience function to change the content of a html element, usually a <code>&lt;div&gt;</code>.
  * @param id ID if the element
  * @param html Contents of the element in HTML format
  */
function setHtml(id,html) {
    document.getElementById(id).innerHTML=html; 
}

/**
  * Called before a page is loaded in order to save DOM content. This is required for correct DOM restoration after restoration of scriptVars
  * This function if empty by default and can be overwritten by user. 
  */
function backupDOM() {
}

function backupDOM1() {
    scriptVars.backupDOM.text    = document.getElementById("text").innerHTML;
    scriptVars.backupDOM.media   = document.getElementById("media").innerHTML;
    scriptVars.backupDOM.audio   = scriptVars.audioSrc;
    scriptVars.backupDOM.mps   	 = scriptVars.mpsObject;
    backupDOM();
}


/**
  * Called after restoration of scriptVars and can be used for things like restoring DOM content.
  * This function if empty by default and can be overwritten by user. 
  */
function restoreDOM() {
}

function restoreDOM1() {
    document.getElementById("text").innerHTML    = scriptVars.backupDOM.text;
    document.getElementById("media").innerHTML   = scriptVars.backupDOM.media;
    const audio = scriptVars.backupDOM.audio;
    if ( audio.length > 0) playAudio(audio);
    else stopAudio();
    const mps = scriptVars.backupDOM.mps;
    if ( typeof(mps) == "object" )
	loadMps(mps);
}


// *****************************************************************************
// ******* media ***************************************************************
// *****************************************************************************
/** 
  * Clears the media area.
  */
function clearMedia() {
    setHtml('media','');
    scriptVars.imageSrc = "";
    scriptVars.videoSrc = "";
    scriptVars.backupDOM.media = "";	// avoids restoring media if media is cleared in current code block
}    


/** 
  * Draws a picture into the media area.
  * @param src File name of the image.
  */
function setImage(src) {
    scriptVars.imageSrc = baseDirectory+"/"+scriptVars.imageDir+'/'+src;
    scriptVars.videoSrc = "";
    console.log("Drawing image '"+scriptVars.imageSrc+"'");
    setHtml('media','<img src="'+scriptVars.imageSrc+'" id="mediaimg">'); 
    scriptVars.backupDOM.media = "";	// avoids restoring media if media is set in current code block
}


/** 
  * Draws a video into the media area.
  * @param src File name of the video.
  */
function setVideo(src) {
    scriptVars.videoSrc = baseDirectory+"/"+scriptVars.videoDir+'/'+src;
    scriptVars.imageSrc = "";
    console.log("Playing video '"+scriptVars.videoSrc+"'");
    setHtml('media','<video id="mediavid" loop="true" autoplay>'+
    '<source src="'+scriptVars.videoSrc+'">'+
    'Your browser does not support HTML5 video.'+
    ' </video>');
    scriptVars.backupDOM.media = "";	// avoids restoring media if media is set in current code block
}    


// *****************************************************************************
// ******* sound ***************************************************************
// *****************************************************************************
var audioElement = null;

/**
  * If true, suffix of audio files is replaced by '.mps' if {@link serverMode} is true.
  */
var audioForceMPS = false;

/**
  * Stops playing sound.
  */
function stopAudio() {
    if ( serverMode ) {
	var hr = new XMLHttpRequest();
	hr.open("GET", "%3AstopAudio", true);		// don't wait or evaluate response 
	hr.send();
    } 
    if ( audioElement !== null ) {
        audioElement.pause();
        audioElement.src = "";
    }
    scriptVars.audioSrc = "";
    scriptVars.mpsObject = {};
    scriptVars.backupDOM.audio = "";			// avoids restoring audio if audio is stopped in current code block
    scriptVars.backupDOM.mps = {};
}

/**
  * Starts playing sound.
  * @param src Name of the sound file.
  */
function playAudio(src) {
    scriptVars.backupDOM.audio = "";			// avoids restoring audio if audio is loaded in current code block
    if ( serverMode && audioForceMPS ) {
	var i = src.lastIndexOf(".");
	src = ( (i<0) ? src : src.substr(0,i) ) + ".mps";
    }
    scriptVars.audioSrc = baseDirectory+"/"+scriptVars.audioDir+'/'+src;
    console.log("Play audio file '" + scriptVars.audioSrc + "'");
    if ( serverMode && audioForceMPS ) {
	var hr = new XMLHttpRequest();
	hr.open("GET", scriptVars.audioSrc, true);	// don't wait or evaluate response 
	hr.send();
    }
    else {
	var s = document.getElementById("sound"); 
	if ( audioElement === null ) {
	    console.log("Creating new audio element");
	    audioElement = document.createElement("audio");
	    audioElement.style.display = "none";
	    if ( typeof(scriptVars.audioVolume) == 'number' )
    		audioElement.volume = scriptVars.audioVolume;
	}
	audioElement.preload = "auto";
	audioElement.controls = false;
	audioElement.autoplay = true;
	audioElement.src = scriptVars.audioSrc;
    }
}

/**
  * Sets the audio volume.
  * @param vol Audio volume
  */
function audioVolume(vol) {
    if ( audioElement !== null ) audioElement.volume = vol;
    console.log("Set volume to " + vol);
    scriptVars.audioVolume = vol;
}


// *****************************************************************************
// ******* text ****************************************************************
// *****************************************************************************
var tButtonNewline = false;


/** 
  * Clears the text area.
  */
function clearText() {
    setHtml('text','');
    tButtonNewline = false;
    scriptVars.backupDOM.text = "";		// avoids restoring text if text is cleared in current code block
}


/** 
  * Set the contents of the text area.
  * @param text HTML formatted text which will be shown.
  */
function setText(text) {
    setHtml('text',text);
    tButtonNewline = true;
    scriptVars.backupDOM.text = "";		// avoids restoring text if text is set in current code block
}


/** 
  * Appends text to the text section.
  * @param text HTML formatted text which will be appended.
  */
function appendText(text) {
    var e = document.getElementById('text');
    var t = e.innerHTML;
    var tt = ("" + text).trim();
    tButtonNewline = tt.length>0;
    if ( ! tButtonNewline ) tt = "&nbsp;";
    e.innerHTML = ( (typeof(t)=='string') && (t.length>0) ) ? t + "<p>" + tt : tt; 
    e.scrollTop = e.scrollHeight;
    console.log("->"+tt+"<-" + tButtonNewline);
}


/**
  * Add a button in text area.
  * @param {String} action One or more commands which will be executed if button is pressed.
  * @param Name The button text.
  */
function appendTButton(action,name) {
    document.getElementById('text').innerHTML += ( tButtonNewline ? '<p>' : '' ) + '<button onclick="' + action + '">'+name+'</button>&nbsp;&nbsp;';
    tButtonNewline = false;
}


// *****************************************************************************
// ******* buttons *************************************************************
// *****************************************************************************

// called every 100ms and updates buttons if updateButtonsRequired is true
function updateButtons() {
    if ( !updateButtonsRequired ) return;
    var text = ""
    for (var i=0; i<scriptVars.buttons.actions.length; i++) {
	var name = scriptVars.buttons.names[i];
	var action = scriptVars.buttons.actions[i];
	if ( typeof(action) == 'string' ) text+='&nbsp;<button onclick="' + action + '">'+name+'</button>';
    }
    setHtml('buttons',text);
    updateButtonsRequired = false;
}


setInterval(updateButtons, 100);

// search for button with that name
function findButton(name) {
    var lname = (""+name).toLowerCase();
    var l = scriptVars.buttons.actions.length;
    for ( var i=0; i<l; i++ ) 
	if ( scriptVars.buttons.names[i].toLowerCase() == lname ) return i;
    return -1;
}


/**
  * Add a button.
  * @param {String} action One or more commands which will be executed if button is pressed.
  * @param Name The button text.
  */
function addButton(action,name) {
    var i = findButton(name);
    if ( i < 0 ) {
	for ( i=0; i<scriptVars.buttons.actions.length; i++ ) 
	    if ( typeof(scriptVars.buttons.actions[i]) != 'string') break;
    }
    scriptVars.buttons.names[i] = name;
    scriptVars.buttons.actions[i] = action;
    updateButtonsRequired = true;
}


/**
  * Removes a button.
  * The button is identified by its name. Case is ignored.
  * @param name The button text.
  */
function rmButton(name) {
    var i = findButton(name);
    if (i >=0 )  {
	scriptVars.buttons.actions[i] = undefined;
	scriptVars.buttons.names[i] = '';
    }
    updateButtonsRequired = true;
}


// *****************************************************************************
// ******* timer / idle ********************************************************
// *****************************************************************************
/**
  * Function called by delay approximately every 250ms. This function can be overwritten
  * by user.
  * @param {number} remaining Remaining wait time in seconds.
  * @param {number} total Total wait time in seconds.
  */
function delayIdle(remaining, total) {
}


var timerText = "";
/**
  * Sets the content of the timer element. DOM if only modified it the text changes.
  * @param t Content for the timer element.
  */
function setTimerContent(t) {
    if ( t != timerText ) {  // only update DOM if tax has been changed
	timerText = t;
	setHtml("timer",timerText);
    }
}

// RTL high level version. Directly called by delay and calls delayIdle
function delayIdle1(remaining, total) {
    setTimerContent( remaining < 500 ? Math.round(remaining+0.499).toString() : "" );
    delayIdle(remaining);
}


// *****************************************************************************
// ******* helper pages ********************************************************
// *****************************************************************************
/**
  * This method is called after script has bee exited.
  * It can be overwritten if desired.
  */
function scriptExitPage() {
    setText("Script has been exited.<br>Reload the page for restart");
    stopAudio();
}


 