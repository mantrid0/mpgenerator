/** @file  rtl0.js
  * This is the low level part of the gasm run time library.
  */

/* todo:
   - errors,warnigs -> gserver
   - line numbers within code blocks 
 */
 
'use strict';

// Compatibility tests.
try {
    let AsyncFunction = Object.getPrototypeOf(async function(){}).constructor;
    if ( (typeof(Promise)!="function" ) || (typeof(AsyncFunction)!="function") ) throw 0;
} 
catch ( e ) {
    alert("Your browser seems to be incompatible: No support for 'Promise' and 'AsyncFunction'.");
}
let AsyncFunction = Object.getPrototypeOf(async function(){}).constructor;

/** 
  * If this it is an non-empty string it is used as filename for saving / restoring states
  * States are auto-saved if this variable is set and script runs from a server
  */
var stateName = null;

/** @namespace
  * All script variables should be stored in this object. Restoring this object should
  * allow re-entry of the script at a saved state.
  */
var scriptVars = {};

/**
  * Contains the index of the currently running code block
  */
scriptVars.currentBlock = 0;

/** 
  * This array forms the execution stack. The last entry contains the index 
  * of the next code block. If this is negative or not a number <code>{@link scriptVars.currentBlock}+1</code> will
  * be used instead. 
  */
scriptVars.nextBlock = [];

/** 
  * If a code block has been visited it is marked with true.
  * That flag can be toggled by user 
  */
scriptVars.blockVisited = [];

/**
  * Current mps object. This describes an audio script in in MPGenerator format.
  * The following keys are evaluated (all keys are optional):
  * <dl> 
  * <dt> <code>vars<code> </dt>
  * <dd> An object containing the user variables (without the leading '@' character)</dd>
  * <dt> <code>scr<code> </dt>
  * <dd> A string containing the script</dd>
  * <dt> <code>fn<code> </dt>
  * <dd> A filename of the script. If both, <code>scr</code> and <code>fn</code> are defined, <code>scr</code> is used and <code>fn</code> can be used as identifier </dd>
  * </dl>
  * If both keys, <code>fn</code> and <code>scr</code> are defined, <code>scr</code> is ignored.
  */
scriptVars.mpsObject = {};

/** 
  * If a script state has been restored automatically from gserver this contains the {@link scriptVars} object of the previous session.
  * It can be used to detect whether the script starts with restored state in order to allow re-entry ({@link restoreScript(scriptVarsPrev)}) or reset ({@link resetScript()})
  */
var scriptVarsPrev = null;




// Index of start block
var startIdx = -1;

// Init low level RTL script variables. Called by initScriptVars0.
function initScriptVars0 () {
    scriptVars = {};
    scriptVars.currentBlock = startIdx;
    scriptVars.nextBlock = [];
    scriptVars.nextBlock[0] = startIdx;
    scriptVars.blockVisited = [];
    scriptVars.mpsObject = {};
    // high level RTL script variables
    initScriptVars1();
}

/** 
  * This variable is automatically set to true if a gserver has been detected
  */
var serverMode = false;


// Tests whether gserver is running
try {
    var hr = new XMLHttpRequest();
    hr.open("GET", "%3AstopAudio", true);
    /** @ignore */
    hr.onload = function () {
	console.log("Detected gserver");
	serverMode = true;
    };
    hr.send(null);
}
catch(exception) {
}


/**
  * Return the index of the current block.
  * This also works if {@link scriptVars.currentBlock} is invalid. In that case 0 is returned, which points to the entry code block.
  */
function currentBlock() {
    if ( (typeof(scriptVars.currentBlock) == 'number') && (scriptVars.currentBlock>=0) ) return scriptVars.currentBlock;
    return 0;
}

    
/**
  * Searches for a code block with a given label and return its index. If no code block is found, -1 is returned.
  * @param label The label of the code block. It is automatically converted to lower case because all labels are lower case.
  */
function findLabel(label) {
    if ( (typeof(label) != 'string') ) return -1;
    if ( label.length < 1 ) return -1;
    var llabel = label.toLowerCase();
    for (var i=0; i<scriptCode.label.length; i++ ) 
	if ( scriptCode.label[i] == label ) return i;
    return -1;
}

/**
  * Prints messages to server log if server is running.
  * @param {String} Message type, supported: "msg", "error", "warning", "info"
  * @param msg The message to print
  */
function serverLog(type,msg) {
    if ( !serverMode ) return;
    var hr = new XMLHttpRequest();
    hr.open("POST", "%3Aprint", true);	// don't wait or evaluate response 
    hr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded" );
    hr.send(encodeURIComponent(type)+"="+encodeURIComponent(msg));
}

// True if we are in script
var runScriptActive = false;

/**
  * Generates a error alert which includes information about the code block where it occurred.
  * @param err The error as Error or as string containing the message
  */
function scriptError(err) {
    var msg;
    if ( runScriptActive ) {
	var idx = currentBlock();
	msg = "Error in code block at " + scriptCode.sourceFN[idx]+"("+scriptCode.sourceLN[idx]+"): ";
    } else {
	msg = "Error: "
    }
    if ( err.message ) {
	msg+=err.message;
	if ( err.stack ) msg+= "\nStack trace:\n" + err.stack;
    }
    else {
	msg+=err;
    }
    console.log(msg);
    serverLog("msg","Script "+msg);
    alert(msg);
}

/**
  * Sets the target for the next page. In particular, this stets the last element in
  * the execution stack {@link scriptVars.nextBlock} to the index of the code block
  * with the given label.
  * In order to implement goto functionality, the code block must be exited using the <code>return;<code> statement.
  * If that function is called during {@link delay} (usually by pressing a button), {@link delay} will exit with return value <code>true</code>.
  * @param label The label of the code block which is executed as next.
  */
function setGoto(label) {
    var idx = findLabel(label);
    if ( idx < 0 ) scriptError("label `" + label + "' does not exist");
    else scriptVars.nextBlock[scriptVars.nextBlock.length-1] = idx;
}


/**
  * Repeat the current code block.
  * In particular, this stets the last element in the execution stack {@link scriptVars.nextBlock} to {@link scriptVars.currentBlock}.
  * In order to implement that functionality, the code block must be exited using the <code>return;<code> statement.
  * If that function is called during {@link delay} (usually by pressing a button), {@link delay} will exit with return value <code>true</code>.
  */
function setGotoRepeat() {
    scriptVars.nextBlock[scriptVars.nextBlock.length-1] = scriptVars.currentBlock;
}

/**
  * Sets the call target for the next page. In particular, this stets the last element in
  * the execution stack {@link scriptVars.nextBlock} to the index of the code block
  * with the label <code>returnLabel</code> and pushes the index given by <code>label</code>
  * to the stack.
  * In order to implement call functionality, the code block must be exited using the <code>return;<code> statement.
  * If that function is called during {@link delay} (usually by pressing a button), {@link delay} will exit with return value <code>true</code>.
  * @param label The label of the code block which is executed as next.
  * @param returnLabel The label of the code block which is the return target. If this is not a string, {@link currentBlock()}+1 is used as default return address.
  */
function setCall(label, returnLabel) {
    var idx;
    if ( typeof(returnLabel) != 'string' ) idx = currentBlock()+1 ;
    else {
	idx = findLabel(returnLabel);
	if ( idx < 0 ) {
	    scriptError("label `" + returnLabel + "' does not exist");
	    return;
	}
    }
    scriptVars.nextBlock[scriptVars.nextBlock.length-1] = idx;
    
    idx = findLabel(label);
    if ( idx < 0 ) scriptError("label `" + label + "' does not exist");
    else scriptVars.nextBlock[scriptVars.nextBlock.length] = idx;
}


/**
  * Returns from a call. 
  * This is done by removing the last element from the execution stack {@link scriptVars.nextBlock};
  * If that function is called during {@link delay} (usually by pressing a button), {@link delay} will exit with return value <code>true</code>.
  */
function callReturn() {
    scriptVars.nextBlock.pop();
}


// Set to true in order to interrupt delay
var delayInterrupt = false;


/**
  * If that function is called during {@link delay} (usually by pressing a button), {@link delay} will exit with return value <code>false</code>.
  */
function interrupt() {
    delayInterrupt = true;
}


/** 
  * This implements the delay function. It waits until either a timeout is reached, {@link interrupt()} is called
  * or execution pointer {@link scriptVar.nextBlock} is modified. In this case it returns <code>true</code>, in all other cases <code>false</code>.
  * <p>
  * This function is asynchronous, i.e. it always must be called using the <code>await</code> operator. In order to implement branching (see {@link setGoto} and {@link setCall})
  * the delay function should be used as follows:
  * <pre class="code">if ( await delay(seconds) ) return; </pre>
  * @param seconds The Time to wait in seconds.
  */
function delay(seconds) {}  // dummy for jsDoc
async function delay(seconds) {
    var waitUntil = new Date().getTime() + ( ( (typeof(seconds) == 'number') && (seconds>=1e-10) ) ? seconds*1000 : 1000*1000 );
    saveState();		// best place to save state on server
    var ep = scriptVars.nextBlock.length-1;
    var nextBlock = null;
    if ( ep < 0 ) ep = 0;
    else {
	nextBlock = scriptVars.nextBlock[ep];
	scriptVars.nextBlock[ep] = -1234;	// in order to detect any assignmnent
    }
    delayInterrupt = false
    var result = false;
    var cnt = 0;
    while ( !delayInterrupt && !result && (new Date().getTime()<waitUntil) ) {
	if ( cnt%5 === 0 ) {
	    delayIdle1((waitUntil - new Date().getTime())*0.001, seconds);
	} 
	cnt++;
	if ( (scriptVars.nextBlock.length==ep+1) && (scriptVars.nextBlock[ep]==-1234) ) {
	    await new Promise(resolve => setTimeout(resolve, 50));		// 50 ms tick
	} else {
	    result = true;
	}
    }
    if ( (scriptVars.nextBlock.length>ep) && (scriptVars.nextBlock[ep] == -1234 ) ) scriptVars.nextBlock[ep] = nextBlock;
    return result;
}


/** 
  * This saves the state on server if {@link stateName} is a non-empty string and script runs in server mode.
  */
function saveState() {
    if ( serverMode && (typeof(stateName)=='string') && (stateName.length>1) ) {
	var hr = new XMLHttpRequest();
	hr.open("POST", "%3Asavejson", true);	// don't wait or evaluate response 
	hr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded" );
	hr.send(encodeURIComponent(stateName)+"="+encodeURIComponent(JSON.stringify(scriptVars)));
    } 
}


/** 
  * This is the main script loop. The script is either exited if the execution stack 
  * {@link scriptVars.nextBlock} becomes empty (usually by using {@link callReturn}
  * or after the last code block has been executed.
  * For starting a script the function {@link startScript} should be used.
  */
function runScript() {}		// to trick jsDoc
async function runScript() {
    if ( typeof(scriptVars.currentBlock) != "number" || startIdx<0 ) {
	scriptError("runScript cannot be called directly");
	return;
    }
    if ( runScriptActive ) {
	scriptError("runScript cannot be called if script is running");
	return;
    }
    runScriptActive = true;
    console.group("Started script");
    var idx = currentBlock();
    while (( idx>=0) && (idx<=scriptCode.code.length) ) {
	var ep = scriptVars.nextBlock.length-1;
	if ( ep < 0 ) ep=0;
	console.group("Running code block " + idx + " label="+scriptCode.label[idx] + " src=" + scriptCode.sourceFN[idx]+"("+scriptCode.sourceLN[idx]+"), ep="+ep );
	scriptVars.currentBlock = idx;
	scriptVars.nextBlock[ep] = -1;
	scriptVars.blockVisited[idx] = true;
	
	try {
 	    backupDOM1();
	    await AsyncFunction(scriptCode.code[idx]+"\n//# sourceURL="+ scriptCode.sourceFN[idx]+"("+scriptCode.sourceLN[idx]+")")();
	}
	catch ( e ) {
	    scriptError( e );
	}
	ep = scriptVars.nextBlock.length-1;
	if ( ep < 0 ) idx=-1;
	else {
	    idx = scriptVars.nextBlock[ep];
	    if ( (typeof(idx)!='number') || (idx<0) ) idx = currentBlock()+1;
	}
	console.groupEnd();
    }
    console.log("Script exited");

    scriptExitPage();
    
    console.groupEnd();
    scriptVars.nextBlock = [];
    runScriptActive = false;
}


/** 
  * This (re-)starts a script. If no start label is given the script starts with the first code block.
  * @param label Optional start label of the script. If it is not a string this argument is ignored.
  */
function startScript(label) {
    if ( typeof(label) == 'string' ) {
	var idx = findLabel(label);
	if ( idx < 0 ) scriptError("Start label `" + label + "' does not exist");
	startIdx = idx;
    } else startIdx = 0;
    initScriptVars0();
    autoRestoreScript();
    runScript();
}

/** 
  * This resets and restarts a script.
  */
function resetScript() {
    console.log("Reset script");
    scriptVarsPrev = null;
    initScriptVars0();
}

// Tries to restore automatically stored state form gserver. This operation is asynchronous. On success script is restarted.
function autoRestoreScript() {
    var hr = new XMLHttpRequest();
    hr.open("GET", "%3Aloadjson?name="+encodeURIComponent(stateName));
    hr.onload = function () {
	if ( hr.responseText.length<2 ) return;  // no error
	var scriptVarsTmp;
	try {
	    scriptVarsPrev = JSON.parse(hr.responseText);
	    initScriptVars0();
	    scriptVars.currentBlock = startIdx;
	    scriptVars.nextBlock = [];
	    scriptVars.nextBlock[0] = startIdx;
	    serverMode = true;
	    console.log("Loaded previous state. Enabling server mode. Restarting script.");
	} catch ( e ) {
	    scriptError( "Error parsing automatically restored state: " + e.message);
	}
    };
    try {
	hr.send();
    }
    catch ( e ) {
        // not an error
    }
}


/** 
  * Restore the state of a script 
  * @param data If it is a string, it is interpreted as JSON object. If it null, nothing happens. If it is an object it is used to set {@link scriptVars} directly.
  */
function restoreScript(data) {
    var scriptVarsTmp = null;
    if ( typeof(data) == "string" ) {
	try {
	    scriptVarsTmp = JSON.parse(data);
	} catch ( e ) {
    	    scriptError( "Error parsing restored state from JSON: " + e.message);
    	}
    }
    else if ( typeof(data) == "object" ) scriptVarsTmp = data;
    else scriptError( "Invalid argument: String or object expected");
    if ( scriptVarsTmp == null ) return;

    initScriptVars0();
    scriptVars = scriptVarsTmp;
    restoreDOM1();
    const ep = scriptVars.nextBlock.length-1;
    if ( ep>=0 ) scriptVars.nextBlock[ep] = scriptVars.currentBlock;
}


/** 
  * Reads the state from a FileList object, i.e. using the <code>&lt;input&gt;</code> method
  * @param files the <code>FileList</code> object which should only contain one <code>File</code> object.
  */
function readStateFromFile(files) {
    for (let i = 0; i < files.length; i++) {
	const file = files[i];
//    if (!file.type.startsWith('text/')){ continue }
	console.log("Reading file '" + file.name + "' (type='"+file.type+"', size="+file.size+")");
    
	const reader = new FileReader();
	reader.onload = function(e) { 
	    restoreScript(e.target.result);
	}
	reader.readAsText(file);
    }
}


/** 
  * Loads an audio script in MPGenerator format. This requires gserver. 
  * @param {Object} mps The script object as described in {@link scriptVars.mpsObject}.
  */
function loadMps(mps) {
    if ( typeof(mps) != "object" ) {
	scriptError("Invalid usage of loadMPS(): object expected as argument");
	return;
    }
    if ( !mps.fn && !mps.scr && !mps.vars ) return; 		// nothing to do
    if ( mps.fn || mps.scr ) scriptVars.mpsObject = mps; 	// if no new script/filename is defined only variables are appended/replaced
    // compose request
    var hr = new XMLHttpRequest();
    hr.open("POST", (mps.fn && !scr) ? (baseDirectory+"/"+scriptVars.audioDir+'/'+mps.fn) : "%3Amps", true);			// don't wait or evaluate response 
    hr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded" );
    var req = "";
    if ( mps.scr ) req="scr="+encodeURIComponent(mps.scr);
    if ( mps.vars && (typeof(mps)=="object") ) {
	for ( var k in mps.vars ) 
	    if ( mps.vars.hasOwnProperty(k) ) {
		if (req.length>0) req += "&";
		req+=encodeURIComponent("@"+k)+"="+encodeURIComponent(mps.vars[k]);
		scriptVars.mpsObject.vars[k] = mps.vars[k];
	    }
    }
    console.log("loadMps(): scriptVars.mpsObject");
    // send request
    try {
	hr.send(req);
    }
    catch ( e ) {
        scriptError( "Error loading audio script: "+ e.message);
    }
}

