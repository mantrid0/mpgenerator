'use strict';

/* 
   Tease specific debug function
   DOM content is only updated if the page changes.
*/   
var lastDebugBlock;

function updateConfig(json) {
    const c = JSON.parse(json);
    document.configForm.configName.value = c.configName;
//    document.configForm.algo.value = c.algo;
    document.configForm.channels.value = c.channels;
    document.configForm.volumeCompensation.value = c.volumeCompensation;
    document.configForm.carrierFrequency.value = c.carrierFrequency;
    document.configForm.dutyRatio.value = c.dutyRatio;
    document.configForm.highFrequency.value = c.highFrequency;
    document.configForm.offsets.value = c.offsets;
    scriptVars.configDir = c.configDir;
}

function configMessage(msg) {
    if ( msg.length > 0) {
	console.log("Configuration messages from server:\n" + msg );
	setHtml("msgs","<h1>Server Messages</h1><pre>" + msg + "</pre>");
    }
}

function sendConfig() {
    var hr = new XMLHttpRequest();
    hr.open("POST", "%3AsetConfig", true);	// don't wait or evaluate response 
    hr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded" );
    hr.send( /* "algo" + "=" + encodeURIComponent(document.configForm.algo.value) + "&" + */
             "channels" + "=" + encodeURIComponent(document.configForm.channels.value) + "&" + 
             "volumeCompensation" + "=" + encodeURIComponent(document.configForm.volumeCompensation.value) + "&" + 
             "carrierFrequency" + "=" + encodeURIComponent(document.configForm.carrierFrequency.value) + "&" + 
             "dutyRatio" + "=" + encodeURIComponent(document.configForm.dutyRatio.value) + "&" + 
             "highFrequency" + "=" + encodeURIComponent(document.configForm.highFrequency.value) + "&" + 
             "offsets" + "=" + encodeURIComponent(document.configForm.offsets.value) + "&"
            );
    hr.onload = function () {
	configMessage(hr.responseText);
    };
}

function setConfigName() {
    var hr = new XMLHttpRequest();
    hr.open("GET", "%3AgetConfig?configName="+encodeURIComponent(document.configForm.configName.value));		// don't wait or evaluate response 
    hr.onload = function () {
	updateConfig(hr.responseText);
    };
    hr.send();
}

function getConfig() {
    var hr = new XMLHttpRequest();
    hr.open("GET", "%3AgetConfig");
    hr.onload = function () {
	updateConfig(hr.responseText);
    };
    hr.send();
}


const ex00_mps = `# Constant signal with 5 s ramp-up. Can be used for calibration.
#repeat -1
    a_a0 = 1
    a_a1 = 1

    run 5

    run 55
#endrepeat`;


const ex01_mps = `# Slowly increasing high frequency effects
#repeat -1
    r=0
    run 3

    run 5

    r=1
    run 20

    run 5

    r=2
    run 20

    run 7
#endrepeat`;


const ex02_mps = `# Slowly increasing phase modulation
#repeat -1
    p=0
    run 3
    
    run 10

    p=1
    p_T=5.5
    run 20

    run 5

    p=2
    run 15

    run 7
#endrepeat`;


const ex03_mps = `# Slowly increasing low frequency effects
#repeat -1
    r=0
    l=0
    run 4

    l=1
    run 22

    run 4
#endrepeat`;

