/** @file utils.js
  * Various helper functions
  */

/* todo:
    - random files selector
*/    

'use strict';

/** 
  * Randomly chooses an existing label from a range.
  * This function is intended to implement pool of labels and randomly choosing one label that has not been visited.
  * If Labels exist but all have been visited, they are set to un-visited first
  * Example:  <pre class="code"><code>findTarget("label",10,20)<code> finds label in the range of target10..target20</pre>
  * @param {String} label Start of the label
  * @param {number} first First element of the range
  * @param {number} last Last element of the range
  */
function findPoolLabel(target,first,last) {
    if ( first == last ) return target+first;
    else if (first>last ) {
	var i = last;
	last = first;
	first = i;
    }
    var flags = [];
    var j = 0, k = 0;
    for ( i=0; i<=last-first; i++ ) {
	var l = findLabel(target+(first+i));
	flags[i] = (l<0) ? 0 : ( scriptVars.blockVisited[l]===true) ? 1 : 2;
    	if ( flags[i] == 2 ) j++;
    	if ( flags[i] > 0 ) k++;
//        console.log("findPoolLabel: " + target+(first+i) + ":  " + flags[i] );
    }
	    
    // no targets found
    var result = target + Math.floor(Math.random()*(last-first)+first);
    if ( k == 0 ) {
	console.error("FindTarget: No targets found, returning: " + result );
	return result;
    }
	    
    // return a unset target
    if ( j > 0 ) {
	j = Math.floor(Math.random()*j);
        for ( i=0; i<=last-first; i++ ) {
            if ( flags[i] == 2 ) {
        	if ( j==0 ) return target+(first+i);
        	j--;
            }
	}
    }

    // unset all valid targets and return a random one
    for ( i=0; i<=last-first; i++ ) {
        if ( flags[i]>0 ) scriptVars.blockVisited[l]=false;
    }
    k = Math.floor(Math.random()*k);
    for ( i=0; i<=last-first; i++ ) {
    	if ( flags[i] > 0  ) {
    	    if ( k==0 ) return target+(first+i);
    	    k--;
    	}
    }

    return result;  // should not happen
}


/** 
  * Randomly chooses an argument.
  */
function chooseFrom() {
    if ( arguments.length < 1 ) return;
    var i = Math.floor(Math.random()*arguments.length);
    if ( i >= arguments.length ) i = arguments.length-1;  // may happen if rounding errors occur
    return arguments[i];
}

