noserver:
    #	if ( serverMode ) @goto("server");
    < 	No gserver found !
    D 	1
    G	noserver

server:
    #	@hl1='<a href="#" onclick="setHtml(\'msgs\',\'<h1>Help</h1>';
    #	@hl2='\'); return false">?</a>';
    <	<form name="configForm"><table> \
<tr><td colspan="4"> <h1>EStim Configuration</h1>Click on the question marks for help. \
<tr><td> Name of the configuration set \
    <td> @hl1 The configuration name defines the directory where configuration and script data is saved.<p> Currently this is <code>'+scriptVars.configDir+'</code> @hl2 \
    <td><input type="text" name="configName"> \
    <td> <button type="button" onclick="setConfigName();">Change</button> \
<tr><td colspan="4"> &nbsp;<br> The following settings come into effect after clicking on one of the test buttons below. There are also templates for most common configurations.\
<tr><td colspan="4" align="center"> <button type="button" onclick="setGoto('stereo');">Stereo Template</button> &nbsp;&nbsp; \
    <button type="button" onclick="setGoto('quad1');">Quad Template 1</button> &nbsp;&nbsp; \
    <button type="button" onclick="setGoto('quad2');">Quad Template 2</button> \
<tr><td> Channel group configuration \
    <td> @hl1 Comma separated list of audio channels per group. \
	<p> In each group one pole of each channel should be connected to a common electrode. It is also possible to connect electrodes of different groups. In that case a single (non-common) electrode of one group should be connected to the common electrode of another group. @hl2 \
    <td><input type="text" name="channels"> \
<tr><td>Phase offsets\
    <td> @hl1 Comma separated list of phase offsets per group. Default values are 0.5. Valid range is 0 to 1. \
	<p> Smaller values increase the current variation between the single (non-common) electrodes. Large values increase the current variation at the common electrode. \
	<p> For testing, use the &quot;Test phase modulation effects&quot;. @hl2 \
    <td><input type="text" name="offsets"> \
<tr><td> Recognition limit in Hz \
    <td> @hl1 This should be the lowest amplitude modulation frequency that feels like a constant signal. \
	 <p> Test it by clicking on the &quot;Test high frequency effects and volume compensation&quot; button. You may reduce the volume compensation during testing.\
	 <p> Typical values are 30 Hz to 50 Hz @hl2 \
    <td><input type="text" name="highFrequency"> \
<tr><td> Volume compensation \
    <td> @hl1 Test it by first adjusting the volume using &quot;Calibrate Volume&quot; button (chose the highest volume that feels comfortable) \
    and then by clicking on the &quot;Test high frequency effects and volume compensation&quot; button:  \
    Chose the largest value that is still comfortable. @hl2 \
    <td><input type="text" name="volumeCompensation"> \
<tr><td> Carrier frequency in Hz\
    <td> @hl1 Should be something between 500 and 1500. <p> Recommended: 1000. <p> Changing this should not have much effect effect. @hl2 \
    <td><input type="text" name="carrierFrequency"> \
<tr><td> Desired duty ratio\
    <td> @hl1 Should be something between 0 and 1. Values less than 1 are only recommended with high-pass filter. If you have one, set it to 0 in order to minimize the energy needed for stimulation. @hl2 \
    <td><input type="text" name="dutyRatio"> \
<tr><td> &nbsp; \
<tr><td colspan="2" align="center"> <button type="button" onclick="setGoto('ex00');">Calibrate Volume</button> \
    <td colspan="2"> (Plain sound) \
<tr><td colspan="2" align="center"> <button type="button" onclick="setGoto('ex01');">Test high frequency effects and volume compensation</button> \
    <td colspan="2"> (Slowly increasing high frequency effects) \
<tr><td colspan="2" align="center"> <button type="button" onclick="setGoto('ex02');">Test phase modulation effects</button> \
    <td colspan="2"> (Slowly increasing phase modulation effects) \
<tr><td colspan="2" align="center"> <button type="button" onclick="setGoto('ex03');">Test low frequency effects</button> \
    <td colspan="2"> (Slowly increasing low frequency effects)\
<tr><td colspan="2" align="center"> <button type="button" onclick="setGoto('save');">Save settings</button> \
    <td colspan="2"> (Save settings without playing sound)\
</table></form>
    # 	getConfig();

form:
    D 	100000

form1:
    BG  form2 Stop Audio
    G 	form

form2:
    A
    RB 	Stop Audio
    G 	form

ex00:
    #	sendConfig();
//    # 	loadMps({ scr : ex00_mps, vars:{a:123,b:234,c:"@@a+@@b"} } );
    # 	loadMps({ scr : ex00_mps });
    G 	form1

ex01:
    #	sendConfig();
    # 	loadMps({ scr : ex01_mps });
    G 	form1


ex02:
    #	sendConfig();
    # 	loadMps({ scr : ex02_mps });
    G 	form1

ex03:
    #	sendConfig();
    # 	loadMps({ scr : ex03_mps });
    G 	form1

save:
    #	sendConfig();
    G 	form

stereo:
    #	setHtml("msgs","<h1>Stereo Template</h1> \
Channels 1 and 2 (left and right) form one 3-phase signal but also can be used in 2-phase setups. 3-phase usually feels better. \
<p> Try to swap the channels and use the configuration that feels softer. \
<p> There are low frequency effects on channels 2 (right). This electrode should be connected to a less sensitive place."); 
    #	document.configForm.channels.value = "2";
    #	document.configForm.offsets.value = "0.5";
    #	document.configForm.volumeCompensation.value = 0.5;
    #	document.configForm.carrierFrequency.value = 1000;
    #	document.configForm.highFrequency.value = 40;
    G 	form

quad1:
    #	setHtml("msgs","<h1>Quad Template 1</h1> \
Channels 1 to 4 (front left, front right, rear left, rear right) form one 5-phase signal. One pole (the same one) of each channel should be connected to a common electrode. \
<p> There are no low frequency effects on channel 1 and low frequency effects are strongest on channel 4. Thus, channel 1 should be connected to the most sensitive place and channel 4 should be connected to the least sensitive place."); 
    #	document.configForm.channels.value = "4";
    #	document.configForm.offsets.value = "0.5";
    #	document.configForm.volumeCompensation.value = 0.5;
    #	document.configForm.carrierFrequency.value = 1000;
    #	document.configForm.highFrequency.value = 40;
    G 	form

quad2:
    #	setHtml("msgs","<h1>Quad Template 2</h1> \
Channels 1 and 2 (front left and front right) form one 3-phase group. Channels 3 and 4 (rear left and rear right) form another 3-phase group. \
You can try to connect common of one group to a non-common electrode of the other group. (If you consider to connect both common electrodes you should use Template 1 instead.) \
<p> There are no low frequency effects on channel 1 and low frequency effects are strongest on channel 4. Thus, channel 1 should be connected to the most sensitive place and channel 4 should be connected to the least sensitive place."); 
    #	document.configForm.channels.value = "2,2";
    #	document.configForm.offsets.value = "0.5,0.5";
    #	document.configForm.volumeCompensation.value = 0.5;
    #	document.configForm.carrierFrequency.value = 1000;
    #	document.configForm.highFrequency.value = 40;
    G 	form
