# Constant effects
#
# $1 duration in s
# $2 volume (0..1)
#
# $3 A-waveform amplitude modulation (0..1)
# $4 A-waveform variation speed in s
#
# $5 HF effects
#
# $6 phase modulation
#
# $7 A-waveform overdrive
#
# $8 LF effects

a_a0 = $2*(1-$3)
a_a1 = $2*(1+$7)
a_a2 = $2
a_tr = $4
a_t0 = 0.1
a_t1 = $4
a_t2 = $4*3
a_n = 2+2*ran

r = $5

p = $6
p_T = 10/(0.6+2.4*ran)

l = $8

run 3

run $1-3


