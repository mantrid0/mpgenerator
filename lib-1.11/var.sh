# normal duration
T=30

# parameter for non-synchronous shocks
ss="2*rnd(ran)-1"

# variations from soft to intense, 4 times normal duration
# $2 minimum number of variations at normal duration
# $3 peak volume 
# $4 peak HF effects
# $5 peak LF effects
# $6 minimum phase modulation
var11 () {
    if [ $i -lt 10 ]; then
	gen $1 var2.mps  $T "($2)*(1+2*ran)"         1      "$3"     1    "0.5*($4)" "1.5*($4)" 1    0 "$5" 1    "($6*0.5)" "($6*2)" 1
    else
	gen $1 var2.mps  "4*$T" "($2)*4*(1+2*ran)"   1 "0.5+($3)" 0.90    "0.5*($4)" "1.5*($4)" 1    0 "$5" 1    "($6*0.5)" "($6*2)" 1
    fi
}    

# variations from soft to intense, reduced periodic variation
# $2 minimum number of variations
# $3 peak volume 
# $4 peak HF effects
# $5 peak LF effects
# $6 minimum phase modulation
var12 () {
    if [ $i -gt 9 ]; then return; fi
    gen $1 var2.mps  $T "($2)*(1+2*ran)"   1 "$3" 1    "0.5*($4)" "1.5*($4)" 0.75    0 "$5" 0    "($6*0.5)" "($6*2)" 0
}    

# transition from normal to painful in 20 steps
# $2 file number
# $3 minimum number of variations
# $4 peak LF effects
# $5 minimum phase modulation
var13 () {
    if [ $i -gt 9 ]; then return; fi
    gen $1 var2.mps  $T "($3)*(1+2*ran)"   1 1.0 1    "($2/19)" "(1+$2/19)" 1   0 "$4" 0   "($5*0.5)" "($5*2)" 1
}


# variations from soft to intense, volume training
# $2 minimum number of variations
# $3 volume training
# $4 peak HF effects
# $5 peak LF effects
# $6 minimum phase modulation
var14 () {
    if [ $i -gt 9 ]; then return; fi
    gen $1 var2.mps  $T "($2)*(1+2*ran)"   "1+($3)*$i/9" "1+($3)*(1+$i/36)" 1    "0.5*($4)" "1.5*($4)" 1   0 "$5" 0    "($6*0.5)" "($6*2)" 1
}


for d in $digits; do
    i=$d
    if [ ! -z "${i#[0-9]*}" ]; then
	i=10
    fi

    # vi1
    var11 vi10$d "1+$i/4.5"  1.0   0.4 "$i/18" "$i/18"

    # v2
    var11 v20$d 3  1.0   0.4 0.5 0.5
    # vv2
    var14 vv20$d 3  0.20  0.4 0.5 0.5

    # v3
    var11 v30$d 3  1.0   0.6 0.5 0.5
    var11 v31$d 3  1.0   0.6 0.3 0.7
    var11 v32$d 3  1.0   0.6   0 1.0
    var12 v33$d 3  1.0   0.6 0.5 0.5
    var12 v34$d 3  1.0   0.6   0 1.0
    # vv3
    var14 vv30$d 3 0.25 0.6 0.6 0.6

    # v4
    var11 v40$d 3  1.0   0.8  0.5 0.5
    var11 v41$d 3  1.0   0.8  0.3 0.7
    var11 v42$d 3  1.0   0.8    0 1.0
    var12 v43$d 3  1.0   0.8  0.5 0.5
    var12 v44$d 3  1.0   0.8  0.3 0.7
    var12 v45$d 3  1.0   0.8    0 1.0
    # vi4
    var13 vi40$d "0$i" "2+ran"  0.7 0.7
    var13 vi41$d "1$i" "2+ran"  0.7 0.7
    # vv4
    var14 vv40$d 3  0.30  0.8 0.8 0.8
    # vp4
    var12 vp40$d 3  1.0   0.8 1.0 0.5

    # v5
    var11 v50$d 3  1.0   1.0 0.5 0.5
    var12 v51$d 3  1.0   1.0 0.5 0.5
    # vi5
    var13 vi50$d "0$i" "2+ran"  1.0 1.0
    var13 vi51$d "1$i" "2+ran"  1.0 1.0
    # vv5
    var14 vv50$d 3  0.40  1.0 1.0 1.0
    # vp5
    var11 vp50$d 3  1.0   1.0 1.0 1.0
    var12 vp51$d 3  1.0   1.0 1.0 1.0
    var12 vp52$d 3  1.0   1.5 0.5 1.0
    # vp0
    var11 vp60$d 3   1.0  1.5 1.0 1.0
    var12 vp61$d 3   1.0  1.5 1.0 1.0
done    


