set -e

install -d $DIR

gen () {
    f=$DIR/$1.$EXT
    if [ ! -f $f ]; then 
	java -jar ../MPGenerator.jar $PARAMS -o $f ${@:2}
    fi	
}

. calib.sh
. normal.sh
. var.sh
. strokes.sh
. shocks.sh
