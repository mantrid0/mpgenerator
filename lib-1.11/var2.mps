# Variations of: 
#   * HF effects (setting r)
#   * LF effects (setting l)
#   * phase modulation (setting p)
#
# $1 duration in s
# $2 number of variations
#
# $3 volume at start
# $4 volume at end
# $5 volume reset factor per variation
#
# $6  HF amplitude modulation at start
# $7  HF amplitude modulation at end
# $8  HF amplitude modulation  reset factor per variation
#
# $9  LF effects at start
# $10 LF effects at end
# $11 LF effects reset factor per variation
#
# $12 phase modulation (0..1) at start
# $13 phase modulation (0..1) at end
# $14 phase modulation 

@n=rnd($2 max 1) 

# A-waveform / volume
@rvol = $5
@dvol = ($4-$3)/(@n-@n*@rvol+@rvol)
@vol = $3 + @dvol*@rvol

# HF effects
@rr = $8
@dr = ($7-$6)/(@n-@n*@rr+@rr)
r = $6 + @dr*@rr

# LF effects
@rl = $11
@dl = ($10-$9)/(@n-@n*@rl+@rl)
l = $9 + @dl*@rl

# phase modulation
@rp = $14
@dp = ($13-$2)/(@n-@n*@rp+@rp)
p = $12 + @dp*@rp
p_T = 10/(0.6+2.4*ran)

# run loop
@vt = $1/@n
repeat @n
    # reset
    @vol = @vol - @dvol*@rvol
    a_a0 = @vol
    a_a1 = @vol
    a_a2 = @vol

    r = r - @dr*@rr

    l = l - @dl*@rl
    
    p = p - @dp*@rp
    

    run 1

    # variation step 
    @vol = @vol + @dvol
    a_a0 = @vol
    a_a1 = @vol
    a_a2 = @vol

    r = r + @dr

    l = l + @dl

    p = p + @dp

    run @vt - 1
    
endrepeat
