# normal duration
T=30

# pause factor or denial version
sdfact=4

# strokes with constant HF effects
# $2 HF effects
# $3 phase modulation
# $4 rise time
# $5 max. number of strokes per sequence
# $6 avg. pause between sequences (multiplied with $sdfact for sd versions)
strokes1 () {
    if [ $i -lt 10 ]; then
	gen s$1  strokes1.mps  $T   1 1    "$2" "$2"    "$3" "$3"    "$4" "($5)*($4)*2*(1+ran)" "$6"  "$5"
	gen sd$1 strokes1.mps  $T   1 1    "$2" "$2"    "$3" "$3"    "$4" "($5)*($4)*2*(1+ran)" "$6*$sdfact"  "$5"
    else 
	gen s$1  strokes1.mps  "4*($T)"  1 1.5    "$2" "$2"    "($3)/12" "($3)/12"    "$4" "($5)*($4)*2*(1+ran)" "$6" "$5"
	gen sd$1 strokes1.mps  "4*($T)"  1 1.5    "$2" "$2"    "($3)/12" "($3)/12"    "$4" "($5)*($4)*2*(1+ran)" "($6)*$sdfact" "$5"
    fi
}

# strokes with increasing periodic modulation
# $2 HF effects
# $3 phase modulation
# $4 rise time
# $5 max. number of strokes per sequence
# $6 avg. pause ratio
# $7 volume increase
strokes2 () {
    if [ $i -gt 9 ]; then return; fi
    gen s$1  strokes1.mps  $T    1 "1+($7)"    "0" "$2"    "0" "$3"    "$4" "($5)*($4)*2*(1+ran)" "$6" "$5"
}


for d in $digits; do
    i=$d
    if [ ! -z "${i#[0-9]*}" ]; then
	i=10
    fi

    # se0, sd0
    strokes1 00$d  0.0 0    	    "2-$i/9" 2       2.0
    # se1, sd1
    strokes1 10$d  0.0 0.2           1.0  "2+$i/3"   2.0
    # se2, sd2
    strokes1 20$d  0.0 0.4  0.5  "3+$i/2"   1.5
    strokes1 21$d  0.4 0.4  1.0  "2+$i/3"   2.0
    # s3, sd3
    strokes1 30$d  0.0 0.7  0.25 "4+$i/1.5" 1.0
    strokes1 31$d  0.4 0.7  0.5  "3+$i/2"   1.5
    strokes1 32$d  0.8 0.7  1.0  "2+$i/3"   2.0
    strokes2 33$d  0.8 1.0  0.5  "3+$i/2"   1.5  0.0
    strokes2 34$d  0.8 1.0  1.0  "2+$i/3"   2.0  0.0
    # s4, sd4
    strokes1 40$d  0.7 1.0  0.5  "3+$i/2"   1.5
    strokes2 41$d  1.0 1.4  0.5  "3+$i/2"   1.5  0.0
    strokes2 42$d  1.0 1.4  1.0  "2+$i/3"   2.0  "0.15+$i/90"
    # s5, sd5
    strokes1 50$d  1.0 1.4 0.25 "4+$i/1.5"  1.0
    strokes2 51$d  1.2 1.7 0.25 "4+$i/1.5"  1.0  0.0
    strokes2 52$d  1.2 1.7  0.5 "3+$i/2"    1.5  "0.2+$i/60"
    # s6, s600...s619 can be used in a row
    strokes2 60$d  1.5 2.0 0.25     "4+$i/1.5" 1.0  "0.3+$i/45"
    strokes2 61$d  1.5 2.0 "0.25-$i/60" "8+$i" 1.0  0.55
done    
