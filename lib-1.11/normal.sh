# normal duration
T=20

# only A-waveform
# $2 A-waveform amplitude modulation (0..1)
# $3 A-waveform variation speed in s
# $4 LF-effects
normal1 () {
    gen $1 normal.mps  $T 1    "$2" "$3"     0 1 0   $4
	
}

# only periodic
# $2 HF effects
# $3 phase modulation
# $4 LF effects
normal2 () {
    gen $1 normal.mps  $T 1    0 1    "$2" "$3"  0  $4
}

# periodic + soft a-waveform 
# $2 HF effects
# $3 phase modulation
# $4 LF effects
normal3 () {
    gen $1 normal.mps  $T 1    0.5 "18/($i+9*ran+9)"    "$2" "$3"   0  $4
}

# periodic + soft a-waveform overdrive
# $2 HF effects
# $3 A-waveform amplitude modulation + overdrive
# $4 LF effects
normal4 () {
    gen $1 normal.mps  $T 1    "$3" "18/($i+9*ran+9)"    "$2" "(1+ran)/2"   0  $3
}


for i in $digits; do
    if [ ! -z "${i#[0-9]*}" ]; then continue; fi

    # ni0: no or only soft 3-phase effects, no randomization
    normal1 ni00$i 0.5 "12/($i+3)"
    normal1 ni01$i "0.5+$i/18" 2 
    normal1 ni02$i "0.5+$i/18" "12/($i+3)" 
    normal2 ni03$i "0.2+$i/30" 0 "0.2+$i/30" 
    # n1
    normal1 n10$i 0.4  "ran/2+9/($i+6)" 		
    normal2 n11$i 0.5  "0.2+$i/30" 
    normal3 n12$i 0.3  "0.2+$i/30" 
    # n2
    normal1 n20$i 1 "ran/3+6/($i+6)" 		
    normal2 n21$i 0.6  0.7
    normal2 n22$i 0.3  1.0
    normal3 n23$i 0.6  0.5
    normal3 n24$i 0.4  0.8
    # ni2
    normal1 ni20$i "0.4+$i/21" "(1+ran)/2" "0.5+($i/30)"
    normal4 ni21$i "0.3+$i/30" "0.5+$i/18"			
    # n3
    normal2 n30$i 0.6   1.0
    normal3 n31$i 0.6   0.8
    # ni3
    normal4 ni30$i "0.4+$i/21" "0.7+$i/18"
done    
